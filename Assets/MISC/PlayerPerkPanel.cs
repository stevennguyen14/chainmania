﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerPerkPanel : MonoBehaviour
{
    public int playerNumber;
    public TextMeshProUGUI playerName;
    public GameObject perkHolder;
    public List<GameObject> perks = new List<GameObject>();
    public TextMeshProUGUI scoreText;
    public RoundController roundController;

    public void SetData(int number)
    {
        if(GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();

        playerNumber = number;
        playerName.text = "Player " + playerNumber;
        scoreText = gameObject.transform.Find("ScoreText").GetComponent<TextMeshProUGUI>();

        if (playerNumber == 1)
            scoreText.text = roundController.player1.transform.GetComponent<PlayerManager>().points.ToString();
        else if (playerNumber == 2)
            scoreText.text = roundController.player2.transform.GetComponent<PlayerManager>().points.ToString();
        else if (playerNumber == 3)
            scoreText.text = roundController.player3.transform.GetComponent<PlayerManager>().points.ToString();
        else if (playerNumber == 4)
            scoreText.text = roundController.player4.transform.GetComponent<PlayerManager>().points.ToString();
        
        AddPerks();
    }

    public void RemovePanel()
    {
        Destroy(gameObject);
    }

    public void AddPerks()
    {
        foreach (GameObject perk in perks)
        {
            Destroy(perk);
        }

        perks.Clear();

        if (playerNumber == 1)
        {
            for (int i = 0; i < roundController.player1Perks.Count; i++)
            {
                GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PerkImage") as GameObject);
                temp.gameObject.transform.SetParent(perkHolder.transform, false);
                SetPerkImage(temp, roundController.player1Perks[i]);
                perks.Add(temp);
            }
        }    
        else if (playerNumber == 2)
        {
            for (int i = 0; i < roundController.player2Perks.Count; i++)
            {
                GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PerkImage") as GameObject);
                temp.gameObject.transform.SetParent(perkHolder.transform, false);
                SetPerkImage(temp, roundController.player2Perks[i]);
                perks.Add(temp);
            }
        }    
        else if (playerNumber == 3)
        {
            for (int i = 0; i < roundController.player3Perks.Count; i++)
            {
                GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PerkImage") as GameObject);
                temp.gameObject.transform.SetParent(perkHolder.transform, false);
                SetPerkImage(temp, roundController.player3Perks[i]);
                perks.Add(temp);
            }
        }    
        else if (playerNumber == 4)
        {
            for (int i = 0; i < roundController.player4Perks.Count; i++)
            {
                GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PerkImage") as GameObject);
                temp.gameObject.transform.SetParent(perkHolder.transform, false);
                SetPerkImage(temp, roundController.player4Perks[i]);
                perks.Add(temp);
            }
        }    
        
    }

    public void SetPerkImage(GameObject temp, string perkName)
    {
        Image image = temp.GetComponent<Image>();

        if (perkName == "Beef")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/LessKnockback") as Sprite;
        }
        else if (perkName == "Titan")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/DamageReduced") as Sprite;
        }
        else if (perkName == "Danger Haste")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/PercentMovespeed") as Sprite;
        }
        else if (perkName == "Danger Impact")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/PercentKnockback") as Sprite;
        }
        else if (perkName == "Hard Hitter")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/IncreaseDamage") as Sprite;
        }
        else if (perkName == "Haste")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/MoveSpeed") as Sprite;
        }
        else if (perkName == "Resilience")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/HealthIncrease") as Sprite;
        }
        else if (perkName == "Impact")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/MoreKnockback") as Sprite;
        }
        else if (perkName == "Inferno")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/DOT") as Sprite;
        }
        else if (perkName == "Ice Cold")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/SlowHit") as Sprite;
        }
        else if (perkName == "Explosive Touch")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/ExplosiveHit") as Sprite;
        }
        else if (perkName == "Phoenix")
        {
            image.sprite = Resources.Load<Sprite>("PerkPrefabs/DontLoseLife") as Sprite;
        }
    }
}
