﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class ObiRopeTrigger : MonoBehaviour
{
	public GameObject explosion;

	//private
	private ObiSolver solver;
	private GameObject owner;
	private AudioClip knockbackSound;
	private AudioSource audioSource;
	private Obi.ObiSolver.ObiCollisionEventArgs collisionEvent;
	private bool canDamage;

	void Awake()
	{
		solver = GetComponent<Obi.ObiSolver>();
	}

	void Start()
	{
		audioSource = Camera.main.GetComponent<AudioSource>();
		knockbackSound = Resources.Load("hit19.mp4") as AudioClip;
		explosion = Resources.Load("explosion_stylized_large_originalFire_noSmoke 1") as GameObject;
		canDamage = true;
	}

	void OnEnable()
	{
		solver.OnCollision += Solver_OnCollision;
	}

	void OnDisable()
	{
		solver.OnCollision -= Solver_OnCollision;
	}

	void Solver_OnCollision(object sender, Obi.ObiSolver.ObiCollisionEventArgs e)
	{
		foreach (Oni.Contact contact in e.contacts)
		{
			// this one is an actual collision:
			if (contact.distance < 0.01)
			{
				Component collider;
				if (ObiCollider.idToCollider.TryGetValue(contact.other, out collider))
				{
					//check if obi rope colliding with other players only before doing something
					if(collider.CompareTag("Player") && collider.gameObject != owner && canDamage)
					{
						//add explosion effect here (knockback is being called in explosion script now) and assign the owner
						Instantiate(explosion, collider.transform.position, Quaternion.identity).GetComponent<Explosion>().AssignOwner(owner);
					
						audioSource.PlayOneShot(knockbackSound);
						canDamage = false;
						//needs are a delay or else the collision can stack up really quick
						StartCoroutine(Delay());
					}
				}
			}
		}
	}

	IEnumerator Delay()
	{
		yield return new WaitForSeconds(0.2f);
		canDamage = true;
	}

	public void AssignOwner(GameObject owner)
	{
		this.owner = owner;
	}
}
