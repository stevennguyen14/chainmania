﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeAbility : MonoBehaviour
{
	//public
	public GameObject owner;
    PlayerController ownerController;
	public GameObject hitParticle;
	public AudioClip hit;
	
	//private
	private AudioSource audioSource;

	void Awake()
	{
		audioSource = Camera.main.GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player") && (col.gameObject != owner))
		{
			col.gameObject.GetComponent<PlayerController>().TakeDamage(Random.Range(3, 12) + owner.GetComponent<PlayerController>().perks.addedDamage, gameObject.transform, ownerController.perks.FireChance(), ownerController.perks.ColdChance(), ownerController.perks.ExplodeChance());
			col.gameObject.GetComponent<PlayerController>().KnockBack(owner.transform.position, owner.GetComponent<PlayerController>().perks.addedKnockback, 80f);
			Instantiate(hitParticle, (col.transform.position + new Vector3(0f, 1.7f, 0f)), col.transform.rotation);
			audioSource.PlayOneShot(hit);
		}
	}

	public void AssignOwner(GameObject owner)
	{
		this.owner = owner;
        ownerController = owner.GetComponent<PlayerController>();
	}
}
