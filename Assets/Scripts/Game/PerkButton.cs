﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PerkButton : MonoBehaviour
{
    TextMeshProUGUI perkNameText;
    public PerkChoices perkChoices;
    public int buttonNumber;
    Button btn;
    Image buttonImage;

    [SerializeField]
    public string perkName;

    private void Awake()
    {
        buttonImage = gameObject.GetComponent<Image>();
        btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        Debug.Log("You have clicked the perk!");
        perkChoices.AddPerk(perkName);
        perkChoices.RemoveButton(buttonNumber);
    }

    public void SetPerkName(string name)
    {
        perkName = name;
        perkNameText = gameObject.transform.Find("PerkNameText").GetComponent<TextMeshProUGUI>();
        SetPerkText();


        if (perkName == "Beef")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/LessKnockback") as Sprite;
        }
        else if (perkName == "Titan")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/DamageReduced") as Sprite;
        }
        else if (perkName == "Danger Haste")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/PercentMovespeed") as Sprite;
        }
        else if (perkName == "Danger Impact")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/PercentKnockback") as Sprite;
        }
        else if (perkName == "Hard Hitter")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/IncreaseDamage") as Sprite;
        }
        else if (perkName == "Haste")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/MoveSpeed") as Sprite;
        }
        else if (perkName == "Resilience")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/HealthIncrease") as Sprite;
        }
        else if (perkName == "Impact")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/MoreKnockback") as Sprite;
        }
        else if (perkName == "Inferno")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/DOT") as Sprite;
        }
        else if (perkName == "Ice Cold")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/SlowHit") as Sprite;
        }
        else if (perkName == "Explosive Touch")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/ExplosiveHit") as Sprite;
        }
        else if (perkName == "Phoenix")
        {
            buttonImage.sprite = Resources.Load<Sprite>("PerkPrefabs/DontLoseLife") as Sprite;
        }
    }

    public void SetPerkText()
    {
        perkNameText.text = perkName;
    }

    public string GetPerkName()
    {
        return perkName;
    }
}

