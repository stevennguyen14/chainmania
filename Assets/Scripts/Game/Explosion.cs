﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
	public float radius;
	public float force;

	private GameObject owner;

    // Start is called before the first frame update
    void Start()
    {
		radius = 10f;
		force = 180f;
		print("owner is " + owner.name);
		Explode();
    }


	public void AssignOwner(GameObject owner)
	{
		this.owner = owner;
	}

	public void Explode()
	{
		Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

		foreach(Collider neabyObjects in colliders)
		{
			if (neabyObjects.CompareTag("Player") && neabyObjects.gameObject != owner)
			{
				PlayerController player = neabyObjects.GetComponent<PlayerController>();
				player.gameObject.GetComponent<PlayerController>().TakeDamage(Random.Range(3, 12), gameObject.transform, false, false, false);
				player.gameObject.GetComponent<PlayerController>().KnockBack(transform.position, 0, 170f);
			}
		}
	}

}
