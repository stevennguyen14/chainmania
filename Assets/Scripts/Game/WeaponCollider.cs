﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollider : MonoBehaviour
{
	public BoxCollider triggerCollider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player"))
		{
			WeaponSystem player = col.gameObject.GetComponent<WeaponSystem>();

			//if not currently holding a weapon, then allow to pickup
			if (!player.HoldingWeapon())
			{
				//sword
				if (gameObject.CompareTag("TwoHandedSword"))
				{
					player.EnableTwoHandedSword();
				}
				//spear
				/*else if (gameObject.CompareTag("TwoHandedSpear"))
				{
					player.EnableTwoHandedSpear();
				}*/
				//bow
				else if (gameObject.CompareTag("Bow"))
				{
					player.EnableBow();
				}
				else if(gameObject.CompareTag("TwoHandedAxe"))
				{
					player.EnableTwoHandedAxe();
				}

				//player.assignWeapon(this.gameObject);
				gameObject.SetActive(false);
			}
		}
	}

	public IEnumerator disableCollider()
	{
		triggerCollider.enabled = false;
		yield return new WaitForSeconds(1f);
		triggerCollider.enabled = true;
	}
}
