﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public Transform camLocation;
    public Renderer rend;
    public Texture redTexture;
    public Texture greenTexture;
    public Texture blueTexture;
    public Texture purpleTexture;

    // Start is called before the first frame update
    void Start()
    {
        transform.LookAt(camLocation);

        if (gameObject.name == "Player 2 Model")
        {
            rend.material.mainTexture = redTexture;
        }


        if (gameObject.name == "Player 3 Model")
        {
            rend.material.mainTexture = greenTexture;
        }


        if (gameObject.name == "Player 4 Model")
        {
            rend.material.mainTexture = blueTexture;
        }

        if(gameObject.name == "Player 1 Model")
        {
            rend.material.mainTexture = purpleTexture;
        }
    }
}
