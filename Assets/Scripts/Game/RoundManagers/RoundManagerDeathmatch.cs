﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;
using System.Linq;

public class RoundManagerDeathmatch : RoundController
{
    StandaloneInputModule inputModule;

    //game Logic
    public int numberOfRounds;
    public int curentRound;

    //ui stuff
    public TextMeshProUGUI gameWonText;

    //score stuff


    //public GameObject[] activePlayers;
    public List<GameObject> activePlayers = new List<GameObject>();

    //map stuff                 NOT USED YET WILL BE USED FOR ERROR CONTROL
    List<string> mapList = new List<string>(new string[] { });

    //public GameObject[] players;


    public int livingPlayers;
    public string winnerName;

    //implement later

    private void Awake()
    {
        blurScreen = GameObject.Find("Canvas/Blur");
        pauseMenu = GameObject.Find("Canvas/Pause Menu");
        pauseMenu.SetActive(false);
        gameWinScreen = GameObject.Find("Game Win Screen");

        inputModule = GameObject.Find("EventSystem").GetComponent<StandaloneInputModule>();
        curentRound = 1;
        numberOfRounds = 4;
        SetUpMaps();
        StartRoundSetup();
        blurScreen.SetActive(false);
        gameWinScreen.SetActive(false);

        lobbyCanvas = GameObject.Find("Canvas");
        perkPreview = GameObject.Find("PerkTutCanvas");
    }

    public void SetUpMaps()
    {
        if (GM.instance.mapName == "Map_1")
        {
            mapList.Add("Map_1");
            mapList.Add("Map_2");
            mapList.Add("Map_3");
            mapList.Add("Map_4");
        }
        else if (GM.instance.mapName == "Map_2")
        {
            mapList.Add("Map_2");
            mapList.Add("Map_4");
            mapList.Add("Map_3");
            mapList.Add("Map_1");
        }
        else if (GM.instance.mapName == "Map_3")
        {
            mapList.Add("Map_3");
            mapList.Add("Map_1");
            mapList.Add("Map_2");
            mapList.Add("Map_4");
        }
        else if (GM.instance.mapName == "Map_4")
        {
            mapList.Add("Map_4");
            mapList.Add("Map_1");
            mapList.Add("Map_2");
            mapList.Add("Map_3");
        }
    }

    public void StartRoundSetup()
    {
        //spawn in players based on GM information
        if (GM.instance != null)
        {
            //spawn in map
            GenerateMap();

            StartCoroutine(WaitForUpdate());

        }
    }

    public void Countdown()
    {
        countdown = Instantiate(Resources.Load("UIPrefabs/RoundStarterCount") as GameObject);
        countingDown = true;
        countdown.transform.SetParent(lobbyCanvas.transform, false);
        perkPreview.SetActive(false);
    }

    public override void StopCountdown(GameObject obj)
    {
        countingDown = false;
        StartCoroutine(TempDestroy(obj));
    }

    IEnumerator TempDestroy(GameObject obj)
    {
        yield return new WaitForSeconds(2);
        Destroy(obj);
    }

    //map generation
    public void GenerateMap()
    {
        //based on GM information generate the map from the resources MapPrefabs folder
        switch (mapList[curentRound - 1])
        {
            case "Map_1":
                map = Instantiate(Resources.Load("MapPrefabs/Map_1") as GameObject);
                break;
            case "Map_2":
                map = Instantiate(Resources.Load("MapPrefabs/Map_2") as GameObject);
                break;
            case "Map_3":
                map = Instantiate(Resources.Load("MapPrefabs/Map_3") as GameObject);
                break;
            case "Map_4":
                map = Instantiate(Resources.Load("MapPrefabs/Map_4") as GameObject);
                break;
        }
    }

    int counter;
    bool gameOver = false;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameOver)
            return;

        //if there is only 1 left set them the winner. 
        if (activePlayers.Count == 1 && !pickingPerks)
        {
            counter = 0;
            winnerName = activePlayers[0].name;
            Destroy(activePlayers[0].transform.parent.GetComponent<PlayerManager>().uiPanel);

            winnerName = "Player " + activePlayers[0].transform.parent.GetComponent<PlayerManager>().playerNumber;
            //gameWonText.gameObject.SetActive(true);
            //gameWonText.text = "Winner! \n" + winnerName;

            playerRankings.Add(activePlayers[0].transform.parent.gameObject);
            activePlayers[0].gameObject.SetActive(false);

            playerRankings.Reverse();

            foreach (GameObject player in playerRankings)
            {
                int scoreToAdd = 0;

                if (counter == 0)
                    scoreToAdd = 100;
                else if (counter == 1)
                    scoreToAdd = 75;
                else if (counter == 2)
                    scoreToAdd = 50;
                else if (counter == 3)
                    scoreToAdd = 25;

                player.transform.GetComponent<PlayerManager>().AddScore(scoreToAdd);

                counter++;
            }

            playerRankings.Reverse();

            if (curentRound >= numberOfRounds)
            {
                StartCoroutine(GameOver());
            }
            else
            {
                curentRound++;
                PickPerks();
            }

        }
    }

    //called when a player dies
    public override void GetLivingPlayers()
    {
        List<GameObject> temp = new List<GameObject>();

        //check to see if players are dead
        foreach (GameObject player in activePlayers)
        {
            if (player.transform.parent.GetComponent<PlayerManager>().lives <= 0)
                temp.Add(player);
        }

        //temp needed to remove items from the list without interupting the loop and causing errors
        foreach (GameObject item in temp)
        {
            activePlayers.Remove(item);
        }
    }

    //temp timer for game over
    IEnumerator GameOver()
    {
        gameOver = true;
        GameOverScoreboard();

        yield return new WaitForSeconds(10);
        SceneManager.LoadScene("MainMenu");

        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    IEnumerator WaitForUpdate()
    {
        yield return new WaitForFixedUpdate();

        spawnPoints = new GameObject[] { };
        spawnPoints = GameObject.FindGameObjectsWithTag("Spawn");

        activePlayers.Clear();

        if (GM.instance.player1Playing)
        {
            player1.SetActive(true);
            player1.transform.GetChild(0).gameObject.SetActive(true);
            player1.transform.GetComponent<PlayerManager>().SetUpPlayer();
            activePlayers.Add(player1.transform.GetChild(0).gameObject);
        }
        if (GM.instance.player2Playing)
        {
            player2.SetActive(true);
            player2.transform.GetChild(0).gameObject.SetActive(true);
            player2.transform.GetComponent<PlayerManager>().SetUpPlayer();
            activePlayers.Add(player2.transform.GetChild(0).gameObject);
        }
        if (GM.instance.player3Playing)
        {
            player3.SetActive(true);
            player3.transform.GetChild(0).gameObject.SetActive(true);
            player3.transform.GetComponent<PlayerManager>().SetUpPlayer();
            activePlayers.Add(player3.transform.GetChild(0).gameObject);
        }
        if (GM.instance.player4Playing)
        {
            player4.SetActive(true);
            player4.transform.GetChild(0).gameObject.SetActive(true);
            player4.transform.GetComponent<PlayerManager>().SetUpPlayer();
            activePlayers.Add(player4.transform.GetChild(0).gameObject);
        }

        //SetPlayers();

        pickingPerks = false;

        playerRankings.Clear();

        Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();
        players.Clear();
        players.AddRange(GameObject.FindGameObjectsWithTag("PlayerController"));

        //spawn in items and weapons
        weaponSpawns = GameObject.FindGameObjectsWithTag("WeaponSpawn");

        Countdown();
    }

    public override void RestartRound()
    {
        //changemap
        Destroy(map.gameObject);
        GenerateMap();

        DeathBox box = GameObject.Find("DeathBox").GetComponent<DeathBox>();
        box.StopAllCoroutines();

        StartCoroutine(WaitForUpdate());
    }
    
    public void PickPerks()
    {
        if (!pickingPerks)
        {
            GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PerksCanvas") as GameObject);
            Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();

            foreach (GameObject player in players)
            {
                player.SetActive(true);
            }

            pickingPerks = true;
        }
    }

    public override void PauseRound(int playerNumber)
    {
        blurScreen.SetActive(true);
        playerWhoPaused = playerNumber;
        roundPaused = true;

        inputModule.horizontalAxis = "HorizontalP" + playerNumber;
        inputModule.verticalAxis = "VerticalP" + playerNumber;
        inputModule.submitButton = "JumpP" + playerNumber;
        inputModule.cancelButton = "ThrowP" + playerNumber;

        pauseMenu.SetActive(true);

        Destroy(countdown);

        StartCoroutine(SelectResumeButton());
        Time.timeScale = 0;
    }

    IEnumerator SelectResumeButton()
    {
        yield return new WaitForEndOfFrame();
        pauseMenu.transform.Find("ResumeButton").GetComponent<Button>().Select();
    }

    public void UnpauseRound()
    {
        Time.timeScale = 1;
        blurScreen.SetActive(false);
        playerWhoPaused = 0;
        StartCoroutine(UnpauseTimer());
        pauseMenu.SetActive(false);

        if (countingDown)
            Countdown();
    }

    IEnumerator UnpauseTimer()
    {
        yield return new WaitForSeconds(1);
        roundPaused = false;
    }

    public void BackToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public GameObject gameWinScreen;

    public TextMeshProUGUI firstPlayer;
    public TextMeshProUGUI secondPlayer;
    public TextMeshProUGUI thirdPlayer;
    public TextMeshProUGUI fourthPlayer;

    public TextMeshProUGUI firstScore;
    public TextMeshProUGUI secondScore;
    public TextMeshProUGUI thirdScore;
    public TextMeshProUGUI fourthtScore;

    public TextMeshProUGUI number3;
    public TextMeshProUGUI number4;

    public void GameOverScoreboard()
    {
        gameWinScreen.SetActive(true);
        blurScreen.SetActive(true);

        players = players.OrderByDescending(o => o.gameObject.transform.GetComponent<PlayerManager>().points).ToList();

        for (int i = 0; i < players.Count; i++)
        {
            if (i == 0)
            {
                firstPlayer.text = "Player " + players[i].transform.GetComponent<PlayerManager>().playerNumber;
                firstScore.text = players[i].transform.GetComponent<PlayerManager>().points.ToString();
            }
            else if (i == 1)
            {
                secondPlayer.text = "Player " + players[i].transform.GetComponent<PlayerManager>().playerNumber;
                secondScore.text = players[i].transform.GetComponent<PlayerManager>().points.ToString();
            }
            else if (i == 2)
            {
                thirdPlayer.text = "Player " + players[i].transform.GetComponent<PlayerManager>().playerNumber;
                thirdScore.text = players[i].transform.GetComponent<PlayerManager>().points.ToString();
            }

            else if (i == 3)
            {
                fourthPlayer.text = "Player " + players[i].transform.GetComponent<PlayerManager>().playerNumber;
                fourthtScore.text = players[i].transform.GetComponent<PlayerManager>().points.ToString();
            }
        }

        if (players.Count == 2)
        {
            thirdPlayer.gameObject.SetActive(false);
            thirdScore.gameObject.SetActive(false);
            fourthPlayer.gameObject.SetActive(false);
            fourthtScore.gameObject.SetActive(false);
            number3.gameObject.SetActive(false);
            number4.gameObject.SetActive(false);
        }
        else if (players.Count == 3)
        {
            fourthPlayer.gameObject.SetActive(false);
            fourthtScore.gameObject.SetActive(false);
            number4.gameObject.SetActive(false);
        }

    }

    public int devPlayer = 1;
    public string testingPerk;

    public void AddTestingPerk(string perkName)
    {
        if (devPlayer == 1)
        {
            player1Perks.Add(perkName);
            if (player1.activeSelf)
                player1.transform.GetComponent<PlayerManager>().ManagePerks();
        }
        else if (devPlayer == 2)
        {
            player2Perks.Add(perkName);
            if (player2.activeSelf)
                player2.transform.GetComponent<PlayerManager>().ManagePerks();
        }
        else if (devPlayer == 3)
        {
            player3Perks.Add(perkName);
            if (player3.activeSelf)
                player3.transform.GetComponent<PlayerManager>().ManagePerks();
        }
        else if (devPlayer == 4)
        {
            player4Perks.Add(perkName);
            if (player4.activeSelf)
                player4.transform.GetComponent<PlayerManager>().ManagePerks();
        }

    }

    public void AddLessKnockback()
    {
        testingPerk = "Beef";
        AddTestingPerk(testingPerk);
    }
    public void AddDamageReduction()
    {
        testingPerk = "Titan";
        AddTestingPerk(testingPerk);
    }
    public void AddPercentMoveSpeed()
    {
        testingPerk = "Danger Haste";
        AddTestingPerk(testingPerk);
    }
    public void AddPercentKnockback()
    {
        testingPerk = "Danger Impact";
        AddTestingPerk(testingPerk);
    }
    public void AddDamage()
    {
        testingPerk = "Hard Hitter";
        AddTestingPerk(testingPerk);
    }
    public void AddMoveSpeed()
    {
        testingPerk = "Haste";
        AddTestingPerk(testingPerk);
    }
    public void AddHealthIncrease()
    {
        testingPerk = "Resilience";
        AddTestingPerk(testingPerk);
    }
    public void AddMoreKnockback()
    {
        testingPerk = "Impact";
        AddTestingPerk(testingPerk);
    }
    public void AddDotOnHit()
    {
        testingPerk = "Inferno";
        AddTestingPerk(testingPerk);
    }
    public void AddSlowOnHit()
    {
        testingPerk = "Ice Cold";
        AddTestingPerk(testingPerk);
    }
    public void AddShockwaveOnHit()
    {
        testingPerk = "Explosive Touch";
        AddTestingPerk(testingPerk);
    }
    public void AddDontLoseLife()
    {
        testingPerk = "Phoenix";
        AddTestingPerk(testingPerk);
    }

    public void SetDevPlayer1()
    {
        devPlayer = 1;
    }
    public void SetDevPlayer2()
    {
        devPlayer = 2;
    }
    public void SetDevPlayer3()
    {
        devPlayer = 3;
    }
    public void SetDevPlayer4()
    {
        devPlayer = 4;
    }

    bool devOpen = false;
    public GameObject devScreen;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            devOpen = true;
            devScreen.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            if (devOpen == true)
            {
                devOpen = false;
                devScreen.SetActive(false);
            }

        }
    }

}
