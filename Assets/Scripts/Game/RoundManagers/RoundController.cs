﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RoundController : MonoBehaviour
{
    private RoundManagerDeathmatch deathmatchRM;

    public string roundType;

    //players
    public List<GameObject> players = new List<GameObject>();
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;

    //perks
    public List<string> player1Perks = new List<string>();
    public List<string> player2Perks = new List<string>();
    public List<string> player3Perks = new List<string>();
    public List<string> player4Perks = new List<string>();

    public GameObject map;
    public GameObject[] spawnPoints;
    public GameObject[] weaponSpawns;

    public List<GameObject> playerRankings = new List<GameObject>();

    public bool countingDown = true;
    public bool pickingPerks = false;
    public int perkPickingPlayer;

    public GameObject countdown;
    public GameObject lobbyCanvas;
    public float roundTimer;

    //pause menu
    public GameObject pauseMenu;
    public GameObject perkPreview;

    public bool roundPaused;
    public int playerWhoPaused;
    public GameObject blurScreen;

    public virtual void RestartRound(){ }
    public virtual void GetLivingPlayers(){ }
    public virtual void StopCountdown(GameObject obj){ }
    public virtual void PauseRound(int playerNumber){ }
}
