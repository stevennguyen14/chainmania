﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class WeaponSystem : MonoBehaviour
{
	//public
	public Animator anim;
	public RuntimeAnimatorController unarmedStance;
	//public RuntimeAnimatorController twoHandedAxeStance;
	//public RuntimeAnimatorController twoHandedSwordStance;
	//public RuntimeAnimatorController twoHandedSpearStance;
	public RuntimeAnimatorController bowStance;
	public float swordReturnTimer;
	public ObiRopeTrigger ropeTriggerScript;
	//weapons
	public GameObject arrow, trap, laser;
	public Transform bowSocket;
	public GameObject twoHandedSword;
	public GameObject twoHandedSwordForThrowing;
	//public GameObject twoHandedSpear;
	//public GameObject twoHandedSpearForThrowing;
	public GameObject twoHandedAxe;
	public GameObject twoHandedAxeForThrowing, axeFX;
	public GameObject bow;
	//public GameObject pickedUpWeap;
	public GameObject ropeRed, ropeOrange, ropeGreen, ropeBlue, axeChain;
	public float axeAbilityDistance;


    public bool holdingBow;
	public bool holdingTwoHandedAxe;
    public bool holdingTwoHandedSword;
    //public bool holdingTwoHandedSpear;
		
	//private
	private PlayerController playerController;
	[SerializeField]
	private string throwButton;
	[SerializeField]
	private string rightTrigger;
	private string leftTrigger;
	private bool holdingWeapon, /*spearAbilityIsReady,*/ returning;
	private ObiRope obiRope;
	private GameObject instantiatedRope, axeRope, iTwoHandedSword, iTwoHandedAxe, iTwoHandedSpear;
	private float timeLeft;
	private bool go = false;
	private Vector3 locationInFrontOfPlayer;
	private AudioSource audioSource;
	private AudioClip axeSound;

    PlayerManager playerManager;
	private void OnEnable()
	{
		twoHandedSword.SetActive(true);
		holdingTwoHandedSword = true;
		twoHandedAxe.SetActive(true);
		holdingTwoHandedAxe = true;

		if (axeRope != null) Destroy(axeRope);
		if (instantiatedRope != null) Destroy(instantiatedRope);
		if(iTwoHandedSword != null) Destroy(iTwoHandedSword);
		if (iTwoHandedAxe != null) Destroy(iTwoHandedAxe);
	}

	// Start is called before the first frame update
	void Awake()
    {
        playerManager = transform.parent.GetComponent<PlayerManager>();

        //assign player controller
        playerController = gameObject.GetComponent<PlayerController>();
		audioSource = Camera.main.GetComponent<AudioSource>();
		axeSound = Resources.Load("DeepWoosh") as AudioClip;

		//set all linked weapon to inactive
		twoHandedSword.SetActive(true);
		//twoHandedSpear.SetActive(false);
		twoHandedAxe.SetActive(true);
		bow.SetActive(false);
		laser.SetActive(false);

		//set the holding weapon to false
		holdingWeapon = false;
		holdingBow = false;
		holdingTwoHandedAxe = true;
		holdingTwoHandedSword = true;
		//spearAbilityIsReady = true;
		returning = false;
		axeFX.SetActive(false);

		//animation controller
		anim = gameObject.GetComponent<Animator>();
		unarmedStance = anim.runtimeAnimatorController;

		throwButton = "ThrowP" + playerManager.playerNumber;
		rightTrigger = "RightTriggerP" + playerManager.playerNumber;
	}

    // Update is called once per frame
    void Update()
    {
		if(Input.GetButtonDown(playerController.xString))
		{
			if (!anim.GetBool("Punch"))
			{
				anim.SetBool("Punch", true);
				//anim.SetTrigger("Punch");
			}

		}

		//if press LT, throw sword ability
		ThrowingSword();
		//if sword thrown then do boomerange
		SwordBoomerang();

		//if holding buttong spin axe
		if (Input.GetButton(playerController.bString) && !playerController.throwing && !playerController.throwingTravel)
		{
			//if holding b button then spin axe ability
			ActivateAxeAbility();
			return;
		}

		//if let go of button then stop
		DeactivateAxeAbility();
    }

	void ThrowingSword()
	{
		//ability
		if (Input.GetAxisRaw(playerController.leftTriggerString) < -0.5f && !playerController.throwing && !playerController.throwingTravel)
		{
			if (holdingTwoHandedSword)
			{
				//sword ability
				EnableTwoHandedSword();
				holdingTwoHandedSword = false;
				anim.SetTrigger("SwordAbility");
			}
		}
	}

	void SwordBoomerang()
	{
		if (iTwoHandedSword != null)
		{
			//if not holding sword then do boomerang stuff
			if (!twoHandedSword.activeSelf && holdingWeapon && !holdingTwoHandedSword && !holdingBow /*&& !holdingTwoHandedSpear*/)
			{
				//Lerp the sword forward and then back torwards owner
				if (go)
				{
					iTwoHandedSword.transform.position = Vector3.MoveTowards(iTwoHandedSword.transform.position, locationInFrontOfPlayer, Time.deltaTime * 10);
				}
				if (!go)
				{
					iTwoHandedSword.transform.position = Vector3.MoveTowards(iTwoHandedSword.transform.position, transform.position, Time.deltaTime * 10);
				}

				//once the sword is close to the player then destroy the instantiated sword and reenable the sword on player hand
				if (!go && Vector3.Distance(transform.position, iTwoHandedSword.transform.position) < 1.5f)
				{
					holdingTwoHandedSword = true;
					Destroy(instantiatedRope);
					Destroy(iTwoHandedSword);
					twoHandedSword.SetActive(true);
				}
			}
		}
	}

	public void PlayAxeSound()
	{
		audioSource.PlayOneShot(axeSound);
	}

	void ActivateAxeAbility()
	{
		//axe ability
		if (holdingTwoHandedAxe)
		{
			holdingTwoHandedAxe = false;
			//TwoHandedAxeAbiliy();
			anim.SetTrigger("AxeAbility");
			anim.SetBool("Spinning", true);
		}

		if (iTwoHandedAxe != null && iTwoHandedAxe.activeSelf)
		{
			gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
			axeFX.SetActive(true);
		}
	}

	void DeactivateAxeAbility()
	{
		//if not holding button then delete
		if (iTwoHandedAxe != null)
		{
			holdingTwoHandedAxe = true;
			Destroy(axeRope);
			Destroy(iTwoHandedAxe);
			twoHandedAxe.SetActive(true);
		}
		anim.SetBool("Spinning", false);
		axeFX.SetActive(false);
	}

	void AssignPinConstraint(ObiCollider obi, Vector3 attachmentOffset)
	{
		// simply add a new pin constraint
		obiRope.PinConstraints.RemoveFromSolver(null);
		ObiPinConstraintBatch batch = (ObiPinConstraintBatch)obiRope.PinConstraints.GetFirstBatch();
		batch.AddConstraint(obiRope.UsedParticles - 1, obi, attachmentOffset, Quaternion.identity, 1);
		obiRope.PinConstraints.AddToSolver(null);
	}

	void RemovePinConstraint()
	{
		// simply remove all pin constraints
		obiRope.PinConstraints.RemoveFromSolver(null);
		obiRope.PinConstraints.GetFirstBatch().Clear();
		obiRope.PinConstraints.AddToSolver(null);
	}

	IEnumerator Boom()
	{
		go = true;
		yield return new WaitForSeconds(1.5f);
		go = false;
	}

	public void ResetWeapon()
	{
		//set all linked weapon to inactive
		twoHandedSword.SetActive(false);
		//twoHandedSpear.SetActive(false);
		twoHandedAxe.SetActive(false);
		bow.SetActive(false);
		laser.SetActive(false);

		//set the holding weapon to false
		holdingWeapon = false;
		//spearAbilityIsReady = true;
		returning = false;
		axeFX.SetActive(false);

		//anim.runtimeAnimatorController = unarmedStance as RuntimeAnimatorController;
	}

	public void TwoHandedSwordAbility()
	{
        if (!playerController.throwing)
        {
            //disable the sword on the player hand
            twoHandedSword.SetActive(false);

            //instantiate an obi rope gameObject
			if(ChainColor() == 1)
				instantiatedRope = Instantiate(ropeOrange, transform.position, Quaternion.identity);
			if (ChainColor() == 2)
				instantiatedRope = Instantiate(ropeRed, transform.position, Quaternion.identity);
			if (ChainColor() == 3)
				instantiatedRope = Instantiate(ropeGreen, transform.position, Quaternion.identity);
			if (ChainColor() == 4)
				instantiatedRope = Instantiate(ropeBlue, transform.position, Quaternion.identity);

			//assign the obi rope 
			obiRope = instantiatedRope.GetComponent<ObiRope>();

            //instantiate an instance of the sword
            iTwoHandedSword = Instantiate(twoHandedSwordForThrowing, ((transform.position + new Vector3(0, 1.6f, 0)) + transform.forward * 1.5f), Quaternion.identity);
            //assign the sword owner
            iTwoHandedSword.GetComponent<SwordAbility>().AssignOwner(this.gameObject);

            //attach rope pin constraint to the sword
            AssignPinConstraint(iTwoHandedSword.GetComponent<ObiCollider>(), new Vector3(0, 0, 0));
            //attach rope to player handle
            GetComponent<PlayerController>().GetObiHandle2().GetComponent<ObiParticleHandle>().Actor = obiRope;

            //get the location in front of player
            locationInFrontOfPlayer = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z) + transform.forward * 10f;

            StartCoroutine(Boom());
        }
	
	}

	int? ChainColor()
	{
		if (gameObject.name == "Player 1")
		{
			return 1;
		}
		if (gameObject.name == "Player 2")
		{
			return 2;
		}
		if (gameObject.name == "Player 3")
		{
			return 3;
		}
		if (gameObject.name == "Player 4")
		{
			return 4;
		}
		else return null;
	}

	/*public void TwoHandedSpearAbility()
	{
		//disable the axe on the player hand
		twoHandedSpear.SetActive(false);

		//instantiate an obi rope gameObject
		instantiatedRope = Instantiate(rope, transform.position, Quaternion.identity);
		instantiatedRope.AddComponent<ObiRopeTrigger>();
		instantiatedRope.GetComponent<ObiRopeTrigger>().AssignOwner(gameObject);
		//assign the obi rope 
		obiRope = instantiatedRope.GetComponent<ObiRope>();

		//instantiate an instance of the axe
		iTwoHandedSpear = Instantiate(twoHandedSpearForThrowing, transform.position + new Vector3(0, 1.6f, 0) + transform.forward * 1.5f, Quaternion.Euler(90, 0, 0));

		//attach rope pin constraint to the sword
		AssignPinConstraint(iTwoHandedSpear.GetComponent<ObiCollider>(), new Vector3(0, 0, 0));
		//attach rope to player handle
		GetComponent<PlayerController>().GetObiHandle2().GetComponent<ObiParticleHandle>().Actor = obiRope;

		//change animation stance
		anim.runtimeAnimatorController = unarmedStance as RuntimeAnimatorController;

		//set spearAbilityIsReady to false
		StartCoroutine(SetSpearAbility(false));
	}*/


	//slight delay so players can't spam the ability
	/*IEnumerator SetSpearAbility(bool isReady)
	{
		yield return new WaitForSeconds(1f);
		spearAbilityIsReady = isReady;
	}*/

	/*IEnumerator ReturnToSpear()
	{
		Vector3 oppDirection = iTwoHandedSpear.transform.position - transform.position;
		oppDirection.y = 0;
		float dist = oppDirection.magnitude;
		
		while (dist > 2f)
		{
			if (iTwoHandedSpear != null)
			{
				oppDirection = iTwoHandedSpear.transform.position - transform.position;
				GetComponent<PlayerController>().rb.AddForce((oppDirection.normalized + (transform.up * 0.3f)) * 15f, ForceMode.Impulse);
				dist = Vector3.Distance(iTwoHandedSpear.transform.position, transform.position);

				//braking after reaching a certain distance
				if (dist < 6f)
				{
					GetComponent<PlayerController>().rb.velocity = GetComponent<PlayerController>().rb.velocity * 0.9f;
				}
			}

			yield return new WaitForFixedUpdate();
		}
		GetComponent<PlayerController>().jump = false;
		holdingTwoHandedSpear = true;
		Destroy(instantiatedRope);
		Destroy(iTwoHandedSpear);
		twoHandedSpear.SetActive(true);
		spearAbilityIsReady = true;
		returning = false;
		//change the anim controller to the correct stance
		anim.runtimeAnimatorController = twoHandedSpearStance as RuntimeAnimatorController;
	}*/

	public void TwoHandedAxeAbiliy()
	{
		//disable the axe on the player hand
		twoHandedAxe.SetActive(false);

		//instantiate an obi rope gameObject
		//instantiatedRope = Instantiate(rope, transform.position, Quaternion.identity);
		axeRope = Instantiate(axeChain, transform.position, Quaternion.identity);
		//assign the obi rope 
		//obiRope = instantiatedRope.GetComponent<ObiRope>();

		//instantiate an instance of the axe
		iTwoHandedAxe = Instantiate(twoHandedAxeForThrowing, ((transform.position + new Vector3(0, 1.6f, 0)) + transform.forward * 3.5f), Quaternion.identity);
		//set the sword as a child of the end chain
		iTwoHandedAxe.transform.parent = axeRope.transform.GetChild(15).transform;
		iTwoHandedAxe.transform.localPosition = new Vector3(0, 0, 0);
		axeRope.transform.parent = GetComponent<PlayerController>().GetObiHandle2().transform;
		axeRope.transform.localPosition = new Vector3(0, 0, 0);

		//assign the axe owner
		iTwoHandedAxe.GetComponent<AxeAbility>().AssignOwner(gameObject);

		//attach rope pin constraint to the sword
		//AssignPinConstraint(iTwoHandedAxe.GetComponent<ObiCollider>(), new Vector3(0, 0, 0));
		//attach rope to player handle
		//GetComponent<PlayerController>().GetObiHandle2().GetComponent<ObiParticleHandle>().Actor = obiRope;

		//change animation stance
		//anim.runtimeAnimatorController = unarmedStance as RuntimeAnimatorController;
	}

	//spawn arrows and add force for the bows
	public void SpawnArrow()
	{
		GameObject arrows = Instantiate(arrow, ((transform.position + new Vector3(0,1.6f,0)) + transform.forward * 2.5f), (transform.rotation * Quaternion.Euler(90,0,0)));
		arrows.GetComponent<Rigidbody>().AddForce(transform.forward * 1600f);
        arrows.GetComponent<Arrow>().owner = gameObject;
		Destroy(arrows, 2f);
	}

	public void SpawnTrap()
	{
		GameObject traps = Instantiate(trap, ((transform.position + new Vector3(0, 1.6f, 0)) + transform.forward * 1.5f), (transform.rotation * Quaternion.Euler(90, 0, 0)));
		traps.GetComponent<Rigidbody>().AddForce(transform.forward * 1000f);
		Destroy(traps, 2f);
	}

	public bool HoldingWeapon()
	{
		return holdingWeapon;
	}

	public void EnableTwoHandedAxe()
	{
		//set weapon to active
		twoHandedAxe.SetActive(true);
		holdingWeapon = true;
		//change the anim controller to the correct stance
		//anim.runtimeAnimatorController = twoHandedAxeStance as RuntimeAnimatorController;

		holdingTwoHandedAxe = true;
	}

	public void EnableTwoHandedSword()
	{
		//set weapon to active
		twoHandedSword.SetActive(true);
		holdingWeapon = true;
		//change the anim controller to the correct stance
		//anim.runtimeAnimatorController = twoHandedSwordStance as RuntimeAnimatorController;

        holdingTwoHandedSword = true;
	}

	/*public void EnableTwoHandedSpear()
	{
		//set weapon to active
		twoHandedSpear.SetActive(true);
		holdingWeapon = true;
		//unarmed is false
		playerController.unarmed = false;
		//change the anim controller to the correct stance
		anim.runtimeAnimatorController = twoHandedSpearStance as RuntimeAnimatorController;

        holdingTwoHandedSpear = true;
	}*/

	public void EnableBow()
	{
		//set weapon to active
		bow.SetActive(true);
		laser.SetActive(true);
		holdingWeapon = true;
		//change the anim controller to the correct stance
		anim.runtimeAnimatorController = bowStance as RuntimeAnimatorController;

        holdingBow = true;
	}

	//weapon throwing
	/*public void throwWeap()
	{
		//deactivate all weapon
		twoHandedSword.SetActive(false);
		twoHandedSpear.SetActive(false);
		twoHandedAxe.SetActive(false);
		bow.SetActive(false);
		laser.SetActive(false);
		holdingWeapon = false;
        holdingBow = false;
        holdingTwoHandedSpear = false;
        holdingTwoHandedSword = false;
		holdingTwoHandedAxe = false;

		//spawn weapon
		pickedUpWeap.transform.position = transform.position + (new Vector3(0, 1.6f, 0) + transform.forward * 1.5f);
		pickedUpWeap.SetActive(true);

		StartCoroutine(pickedUpWeap.GetComponent<WeaponCollider>().disableCollider());
		pickedUpWeap.GetComponent<Rigidbody>().AddForce((transform.forward * 10) + (transform.up * 3), ForceMode.Impulse);
		anim.runtimeAnimatorController = unarmedStance as RuntimeAnimatorController;

		playerController.unarmed = true;
	}

	public void assignWeapon(GameObject weap)
	{
		pickedUpWeap = weap;
	}*/
}
