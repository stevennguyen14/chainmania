﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perks : MonoBehaviour
{
    public PlayerManager playerManager;
    public ParticleManager particleManager;
    public RoundController roundController;

    List<string> perks = new List<string>();

    private void Awake()
    {
        playerManager = gameObject.GetComponent<PlayerManager>();
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();
        //particleManager = transform.Find("Player " + playerManager.playerNumber).GetComponent<ParticleManager>();
    }

    public void GetPerks()
    {
        if (playerManager.playerNumber == 1)
            perks = roundController.player1Perks;
        else if (playerManager.playerNumber == 2)
            perks = roundController.player2Perks;
        else if (playerManager.playerNumber == 3)
            perks = roundController.player3Perks;
        else if (playerManager.playerNumber == 4)
            perks = roundController.player4Perks;
    }

    public int damageResistance = 0;
    public float lessKnockback = 0;
    public int addedDamage = 0;
    public float addedSpeed = 0;
    public float addedKnockback = 0;

    public float moveSpeedThreshold = GameData.MOVESPEEDTHRESHOLD;
    public float moreKnockbackThreshold = GameData.MOREKNOCKBACKTHRESHOLD;

    public float moveSPeedWhileDamaged = 0;
    public float moreKnockbackWhileDamaged = 0;

    public bool addedMoreKnockback = false;
    public bool addedMoreSpeed = false;

    public bool dotOnHit = false;
    public float chanceToDotOnHit = 0;
    public bool slowOnHit = false;
    public float chanceToSlowOnHit = 0;
    public bool explodeOnHit = false;
    public float chanceToExplodeOnHit = 0;
    public float extraLifeChance = 0;

    public void ResetPerks()
    {
        damageResistance = 0;
        lessKnockback = 0;
        addedDamage = 0;
        addedSpeed = 0;
        addedKnockback = 0;

        playerManager.maxHealth = 0;

        moveSpeedThreshold = 90;
        moreKnockbackThreshold = 90;
        moveSPeedWhileDamaged = 0;
        moreKnockbackWhileDamaged = 0;

        addedMoreKnockback = false;
        addedMoreSpeed = false;

        dotOnHit = false;
        chanceToDotOnHit = 0;
        slowOnHit = false;
        chanceToSlowOnHit = 0;
        explodeOnHit = false;
        chanceToExplodeOnHit = 0;
        extraLifeChance = 0;
    }

    public void SetPerks()
    {
        foreach (string perk in perks)
        {
            if (string.Equals(perk, "Beef"))
            {
                //complete
                lessKnockback += 0.5f;
                particleManager.TurnOnLessKnockback();
            }
            else if (string.Equals(perk, "Titan"))
            {
                //complete
                damageResistance += 2;
                particleManager.TurnOnDamageReduction();
            }
            else if (string.Equals(perk, "Danger Haste"))
            {
                //complate
                moveSpeedThreshold -= 10;
                moveSPeedWhileDamaged += 2;
            }
            else if (string.Equals(perk, "Danger Impact"))
            {
                //complete
                moreKnockbackThreshold -= 10;
                moreKnockbackWhileDamaged += 2; //TEST 
            }
            else if (string.Equals(perk, "Hard Hitter"))
            {
                //complete
                addedDamage += 4;
                particleManager.RedHands();
            }
            else if (string.Equals(perk, "Haste"))
            {
                //complate
                addedSpeed += 1f;
                particleManager.TurnOnSpeedLines();
            }
            else if (string.Equals(perk, "Resilience"))
            {
                playerManager.maxHealth += 20;
                playerManager.health = GameData.BASEHEALTH + playerManager.maxHealth;
                playerManager.UpdateHealth();
            }
            else if (string.Equals(perk, "Impact"))
            {
                //complete
                addedKnockback += 1.5f; //TEST
                particleManager.TurnOnGlowingHands();
            }
            else if (string.Equals(perk, "Inferno"))
            {
                //complete
                chanceToDotOnHit += 20;
                dotOnHit = true;
                particleManager.TurnOnFireHands();
            }
            else if (string.Equals(perk, "Ice Cold"))
            {
                //complete
                chanceToSlowOnHit += 20;
                slowOnHit = true;
                particleManager.TurnOnIceHands();
            }
            else if (string.Equals(perk, "Explosive Touch"))
            {
                //complete
                chanceToExplodeOnHit += 20;
                explodeOnHit = true;
            }
            else if (string.Equals(perk, "Phoenix"))
            {
                //complete
                extraLifeChance += 5;
            }
        }
    }

    public bool FireChance()
    {
        int number = Random.Range(1, 100);

        if (number <= chanceToDotOnHit)
            return true;
        else
            return false;
    }

    public bool ColdChance()
    {
        int number = Random.Range(1, 100);

        if (number <= chanceToSlowOnHit)
            return true;
        else
            return false;
    }

    public bool ExplodeChance()
    {
        int number = Random.Range(1, 100);

        if (number <= chanceToExplodeOnHit)
            return true;
        else
            return false;
    }

    public bool FreeRevive()
    {
        int number = Random.Range(1, 100);

        if (number <= extraLifeChance)
            return true;
        else
            return false;
    }

    public void TurnOffParticles()
    {
        particleManager.TurnOffFireHands();
        particleManager.TurnOffIceHands();
        particleManager.TurnOffSpeedLines();
        particleManager.TurnOffGlowingHands();
        particleManager.TurnOffDamageReduction();
        particleManager.TurnOffLessKnockback();
        particleManager.TurnOffAngelWings();
        particleManager.NotOnFire();
        particleManager.NotChilled();
    }
}
