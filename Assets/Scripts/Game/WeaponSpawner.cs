﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawner : MonoBehaviour
{
    bool hasWeapon;
    bool spawningWeapon;
    bool timerDone;
    float timer;
    float timeTillSpawn = 5f;
    GameObject[] weapons;

    // Start is called before the first frame update
    void Start()
    {
        hasWeapon = false;
    }

    // Update is called once per frame
    void Update()
    {
        //if the spawner does not have a weapon start spawning a new one
        if (!hasWeapon)
        {
            spawningWeapon = true;
        }

        //if spawning weapon count down till spawn is available
        if (spawningWeapon)
        {
            timer += Time.deltaTime;
            if (timer >= timeTillSpawn)
            {
                timerDone = true;
                spawningWeapon = false;
                timer = 0;
            }
        }

        //when the timer is done remove the timer and spawn the weapon
        if (timerDone)
        {
            SpawnRandomWeapon();
            timerDone = false;
        }
    }

    //spawns a random weapon and puts it on the spawn point
    public void SpawnRandomWeapon()
    {
        int choice = Random.Range(0, 4);
        GameObject weapon;

        //randomly spawn 1 of the weapons and add it as a child of the spawn point
        switch (choice)
        {
            case 0:
                weapon = Instantiate(Resources.Load("WeaponPrefabs/GreatSword") as GameObject, transform.position, Quaternion.identity);
                weapon.transform.parent = gameObject.transform;
                break;
            case 1:
                weapon = Instantiate(Resources.Load("WeaponPrefabs/Bow") as GameObject, transform.position, Quaternion.identity);
                weapon.transform.parent = gameObject.transform;
                break;
            case 2:
                weapon = Instantiate(Resources.Load("WeaponPrefabs/Spear") as GameObject, transform.position, Quaternion.identity);
                weapon.transform.parent = gameObject.transform;
                break;
			case 3:
				weapon = Instantiate(Resources.Load("WeaponPrefabs/GreatAxe") as GameObject, transform.position, Quaternion.identity);
				weapon.transform.parent = gameObject.transform;
				break;
		}

        //reset all variables 
        hasWeapon = true;
        spawningWeapon = false;
    }

    //called when the player takes the weapon
    public void TakeWeapon()
    {
        hasWeapon = false;
    }
}
