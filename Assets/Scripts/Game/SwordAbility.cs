﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAbility : MonoBehaviour
{
	public GameObject owner;
    PlayerController ownerController;
	public GameObject hitParticle;
	public AudioClip hit, woosh;
	public Material green, orange, red, blue;

	private Rigidbody rb;
	private AudioSource audioSource;
	private MeshRenderer tornaroMesh;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		audioSource = Camera.main.GetComponent<AudioSource>();
		woosh = Resources.Load("DeepWoosh") as AudioClip;
		tornaroMesh = gameObject.transform.Find("Tornado_FX").Find("Tornado_mesh").gameObject.GetComponent<MeshRenderer>();

		if (AbilityColor() == 1)
			tornaroMesh.material = orange;
		else if (AbilityColor() == 2)
			tornaroMesh.material = red;
		else if (AbilityColor() == 3)
			tornaroMesh.material = green;
		else if (AbilityColor() == 4)
			tornaroMesh.material = blue;
	}

	void FixedUpdate()
	{
		transform.Rotate(0, 40f, 0, Space.World);
	}

	void Update()
	{
		//play rotation sound 
		if (transform.eulerAngles.y % 320 == 0)
		{
			audioSource.PlayOneShot(woosh);
		}
	}

    void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player") && (col.gameObject != owner))
		{
			col.gameObject.GetComponent<PlayerController>().TakeDamage(Random.Range(3, 12) + owner.GetComponent<PlayerController>().perks.addedDamage, this.gameObject.transform, ownerController.perks.FireChance(), ownerController.perks.ColdChance(), ownerController.perks.ExplodeChance());
			Instantiate(hitParticle, (col.transform.position + new Vector3(0f, 1.7f, 0f)), col.transform.rotation);
			audioSource.PlayOneShot(hit);
		}
	}

	public void AssignOwner(GameObject owner)
	{
		this.owner = owner;
        ownerController = owner.GetComponent<PlayerController>();
	}

	int? AbilityColor()
	{
		if (owner.name == "Player 1")
		{
			return 1;
		}
		if (owner.name == "Player 2")
		{
			return 2;
		}
		if (owner.name == "Player 3")
		{
			return 3;
		}
		if (owner.name == "Player 4")
		{
			return 4;
		}
		else return null;
	}
}
