﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RoundCountdown : MonoBehaviour
{
    TextMeshProUGUI numbersText;

    float timeLeft = 1f;
    int countdown;
    bool startGame = false;

    RoundController roundController;

    // Start is called before the first frame update
    void Start()
    {
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();

        numbersText = gameObject.transform.Find("CountDownNumbers").GetComponent<TextMeshProUGUI>();
        numbersText.text = "3";
        countdown = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (!startGame)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                countdown--;
                numbersText.text = countdown.ToString();
                timeLeft = 1f;
            }

            if (countdown == 0)
            {
                startGame = true;
                numbersText.text = "Start!";
                roundController.StopCountdown(gameObject);
            }

            if (roundController.roundPaused)
            {
                Destroy(gameObject);
            }
        }
    }
}
