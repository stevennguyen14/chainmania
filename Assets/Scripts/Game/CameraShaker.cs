﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
	[SerializeField]
	private float shakeDuration = 0.1f;

	//camera shake on knockback
	public IEnumerator CameraShake(float shakeAmount)
	{
		Vector3 originalPos = transform.localPosition;

		float t = shakeDuration;

		while (t > 0)
		{
			t -= Time.deltaTime;
			transform.localPosition = Random.insideUnitSphere * shakeAmount;
			yield return null;
		}

		transform.localPosition = originalPos;
	}
}
