﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBox : MonoBehaviour
{
    public RoundController roundController;

    private void Start()
    {
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();
    }

    public void OnTriggerExit(Collider other)
    {
        //if the object is a player
        if (other.gameObject.tag == "Player")
        {
            //cache the player so we can actiate it later
            PlayerController cont  = other.GetComponent<PlayerController>();

			//reset all the weapon
			cont.gameObject.GetComponent<WeaponSystem>().ResetWeapon();

			//function to check if the player is out of lives and deactivate them
			//other.SendMessage("Death");

            other.transform.parent.GetComponent<PlayerManager>().Death();

            //if the player still has lives
            if (cont.playerManager.lives > 0)
            {
                //temp deactivate player 
                cont.gameObject.SetActive(false);
                //reset camera 
                Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();
                StartCoroutine(SpawnDelay(cont.gameObject, 2));
            }
        }
    }
	
	public void StartSpawnDelayCoroutine(GameObject player, int delay)
	{
		StartCoroutine(SpawnDelay(player, delay));
	}

    IEnumerator SpawnDelay(GameObject player, int delay)
    {
        yield return new WaitForSeconds(delay);
        //after delay, activate the playing and camera again 
        player.SetActive(true);

        Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();

        //reset velocity so player spawns in without any momentum
        player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        Transform spawnPoint = null;
        List<Transform> possibleSpaws = new List<Transform>();

        //loop through spawn points and players and see if a player is within a certain distance of the the spawn point
        //if a player is close dont add to the final list of spawn points
        foreach (GameObject spawn in roundController.spawnPoints)
        {
            foreach (GameObject players in roundController.players)
            {
                spawnPoint = spawn.transform;

                if (TargetDistance(spawn, players.transform.GetChild(0).gameObject) < 10)
                {
                    spawnPoint = null;
                    break;
                }
            }

            if (spawnPoint != null)
            {
                possibleSpaws.Add(spawnPoint);
                spawnPoint = null;
            }   
        }

        //if there are no good spawn points spawn randomly otherwise spawn randomly out of the possible good spawn points
        if (possibleSpaws.Count == 0)
            player.transform.position = roundController.spawnPoints[Random.Range(0, roundController.spawnPoints.Length)].transform.position;
        else
            player.transform.position = possibleSpaws[Random.Range(0,possibleSpaws.Count)].position;

        player.transform.parent.GetComponent<PlayerManager>().ManagePerks();

        player.GetComponent<PlayerController>().SetSpawnProtection();
    }

    //function to return the distance between 2 objects
    float TargetDistance(GameObject obj1, GameObject obj2)
    {
        float distance = Vector3.Distance(obj1.transform.position, obj2.transform.position);
        return distance;
    }
}
