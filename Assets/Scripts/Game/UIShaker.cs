﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIShaker : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI health;
	[SerializeField]
	private float shakeDuration = 0.5f;

	private float originalFont;

	void Awake()
	{
		health = transform.Find("Health").GetComponent<TextMeshProUGUI>();
		originalFont = health.fontSize;
	}

	//camera shake on knockback
	public IEnumerator UIShake(float shakeAmount)
	{
		Vector3 originalPos = transform.localPosition;

		float t = shakeDuration;

		while (t > 0)
		{
			t -= Time.deltaTime;
			transform.localPosition = Random.insideUnitSphere * shakeAmount;
			health.fontSize = 80;
			health.color = Color.red;
			yield return null;
		}

		transform.localPosition = originalPos;
		health.color = Color.white;
		health.fontSize = originalFont;
	}
}
