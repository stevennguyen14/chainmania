﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public GameObject owner;
    public PlayerController ownerController;
	public AudioClip explosionSound;

	private GameObject explosion;
	private AudioSource audioSource;

	void Awake()
	{
		explosion = Resources.Load("explosion_stylized_large_originalFire_noSmoke 1") as GameObject;
		audioSource = Camera.main.GetComponent<AudioSource>();
	}

    void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.CompareTag("Player"))
		{
            ownerController = owner.GetComponent<PlayerController>();

			//col.gameObject.GetComponent<PlayerController>().TakeDamage((10 + ownerController.addedDamage), transform.position, ownerController.FireChance(), ownerController.ColdChance(), ownerController.ExplodeChance());
           //col.gameObject.GetComponent<PlayerController>().KnockBack(transform.position, (owner.GetComponent<PlayerController>().addedKnockback + owner.GetComponent<PlayerController>().moreKnockbackWhileDamaged));
			Instantiate(explosion, this.transform);
			audioSource.PlayOneShot(explosionSound);
			StartCoroutine(Camera.main.GetComponent<CameraShaker>().CameraShake(0.8f));
			Destroy(gameObject, 1f);
		}
	}
}
