﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    //FIRE *
    public GameObject firehandsL;
    public GameObject firehandsR;
    public GameObject onFire;
    //ICE *
    public GameObject iceHandsL;
    public GameObject iceHandsR;
    public GameObject chilled;
    //MOVEMENT SPEED * 
    public GameObject speedLines;
    //DAMAGE *
    public GameObject greenFistsL;
    public GameObject greenFistsR;
    public GameObject redFistsL;
    public GameObject redFistsR;
    //KOCKBACK * 
    public GameObject glowFistsL;
    public GameObject glowFistsR;
    //LESS KNOCKBACK * 
    public GameObject knockbackShields;
    //DAMAGE REDUCTION * 
    public GameObject damageShields;
    //NOT LOSING A LIFE * 
    public GameObject angelWings;

    //FIRE
    public void TurnOnFireHands()
    {
        firehandsL.SetActive(true);
        firehandsR.SetActive(true);
    }

    public void TurnOffFireHands()
    {
        firehandsL.SetActive(false);
        firehandsR.SetActive(false);
    }

    public void OnFire()
    {
        onFire.SetActive(true);
        StartCoroutine(FireTimer());
    }

    IEnumerator FireTimer()
    {
        yield return new WaitForSeconds(5);
        NotOnFire();
    }

    public void NotOnFire()
    {
        onFire.SetActive(false);
    }
    //-----------------------
    //ICE
    public void TurnOnIceHands()
    {
        iceHandsL.SetActive(true);
        iceHandsR.SetActive(true);
    }

    public void TurnOffIceHands()
    {
        iceHandsL.SetActive(false);
        iceHandsR.SetActive(false);
    }

    public void Chilled()
    {
        chilled.SetActive(true);
    }

    IEnumerator TimeChilled()
    {
        yield return new WaitForSeconds(5);
        NotChilled();
    }

    public void NotChilled()
    {
        chilled.SetActive(false);
    }
    //-----------------------
    //MOVEMENT SPEED
    public void TurnOnSpeedLines()
    {
        speedLines.SetActive(true);
    }

    public void TurnOffSpeedLines()
    {
        speedLines.SetActive(false);
    }
    //-----------------------
    //DAMAGE
    public void GreenHands()
    {
        greenFistsL.SetActive(true);
        greenFistsR.SetActive(true);
        redFistsL.SetActive(false);
        redFistsR.SetActive(false);
    }

    public void RedHands()
    {
        greenFistsL.SetActive(false);
        greenFistsR.SetActive(false);
        redFistsL.SetActive(true);
        redFistsR.SetActive(true);
    }
    //-----------------------
    //MORE KNOCKBACK
    public void TurnOnGlowingHands()
    {
        glowFistsL.SetActive(true);
        glowFistsR.SetActive(true);
    }

    public void TurnOffGlowingHands()
    {
        glowFistsL.SetActive(false);
        glowFistsR.SetActive(false);
    }
    //-----------------------
    //LESS KNOCKBACK
    public void TurnOnLessKnockback()
    {
        knockbackShields.SetActive(true);
    }

    public void TurnOffLessKnockback()
    {
        knockbackShields.SetActive(false);
    }
    //-----------------------
    //DAMAGE REDUCTION
    public void TurnOnDamageReduction()
    {
        damageShields.SetActive(true);
    }

    public void TurnOffDamageReduction()
    {
        damageShields.SetActive(false);
    }
    //-----------------------
    //NOT LOSING A LIFE
    public void TurnOnAngelWings()
    {
        angelWings.SetActive(true);
        StartCoroutine(AngelTimer());
    }

    IEnumerator AngelTimer()
    {
        yield return new WaitForSeconds(1);
        TurnOffAngelWings();
    }

    public void TurnOffAngelWings()
    {
        angelWings.SetActive(false);
    }

}
