﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class PerkChoices : MonoBehaviour
{
    GameObject perkPanel;
    GameObject playerPanel;
    static int numberOfPerks = 5;
    public List<string> perks = new List<string>();
    PerkButton perkScript;
    List<Sprite> perkImages = new List<Sprite>();

    public List<Button> perkButtons = new List<Button>();
    public List<GameObject> playerPanels = new List<GameObject>();

    int choiceNumber = 0;

    public TextMeshProUGUI choosingName;

    StandaloneInputModule inputModule;

    public RoundController roundController;

    void OnEnable()
    {
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();

        inputModule = GameObject.Find("EventSystem").GetComponent<StandaloneInputModule>();
        choiceNumber = 0;
        perkPanel = gameObject.transform.Find("PerkHolder").gameObject;
        playerPanel = gameObject.transform.Find("PlayerPanels").gameObject;

        RelayPick();
        perks.Clear();
        GeneratePerks();
        SpawnPerks();
        GeneratePlayers();
        SetPlayerChoosing();
        ChangePlayerName();

        perkButtons[0].Select();
    }

    bool disabled;

    private void Update()
    {
        if (!disabled && roundController.perkPreview.activeSelf)
        {
            DisableButtons();
            disabled = true;
        }

        if (disabled && !roundController.perkPreview.activeSelf)
        {
            EnableButtons();
            perkButtons[1].Select();
            perkButtons[0].Select();
            disabled = false;
        }
    }

    void RelayPick()
    {
        roundController.perkPickingPlayer = roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber;
    }

    public void AddPerk(string perk)
    {
        if (roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber == 1)
        {
            roundController.player1Perks.Add(perk);
            foreach (GameObject panel in playerPanels)
            {
                if (panel.GetComponent<PlayerPerkPanel>().playerNumber == 1)
                    panel.GetComponent<PlayerPerkPanel>().AddPerks();
            }
        }
        else if (roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber == 2)
        {
            roundController.player2Perks.Add(perk);
            foreach (GameObject panel in playerPanels)
            {
                if (panel.GetComponent<PlayerPerkPanel>().playerNumber == 2)
                    panel.GetComponent<PlayerPerkPanel>().AddPerks();
            }
        }
        else if (roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber == 3)
        {
            roundController.player3Perks.Add(perk);
            foreach (GameObject panel in playerPanels)
            {
                if (panel.GetComponent<PlayerPerkPanel>().playerNumber == 3)
                    panel.GetComponent<PlayerPerkPanel>().AddPerks();
            }
        }
        else if (roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber == 4)
        {
            roundController.player4Perks.Add(perk);
            foreach (GameObject panel in playerPanels)
            {
                if (panel.GetComponent<PlayerPerkPanel>().playerNumber == 4)
                    panel.GetComponent<PlayerPerkPanel>().AddPerks();
            }
        }

        NextPerson();
    }

    void NextPerson()
    {
        choiceNumber++;
        DisableButtons();
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);

        if (choiceNumber == roundController.players.Count)
        {
            roundController.RestartRound();
            foreach (GameObject obj in playerPanels)
            {
                obj.GetComponent<PlayerPerkPanel>().RemovePanel();
            }

            Destroy(gameObject);
            choiceNumber = 0;
            yield break;
        }

        RelayPick();
        SetPlayerChoosing();
        ChangePlayerName();
        perkButtons[1].Select();
        perkButtons[0].Select();
        EnableButtons();
    }

    void ChangePlayerName()
    {
        choosingName.text = "Player " + roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber + " Selecting";
    }

    void SetPlayerChoosing()
    {
        inputModule.horizontalAxis = "HorizontalP" + roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber;
        inputModule.verticalAxis = "VerticalP" + roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber;
        inputModule.submitButton = "JumpP" + roundController.playerRankings[choiceNumber].transform.GetComponent<PlayerManager>().playerNumber;
    }

    void GeneratePlayers()
    {
        playerPanels.Clear();

        for(int i = 0; i < roundController.playerRankings.Count; i++)
        {
            GameObject temp = Instantiate(Resources.Load("PerkPrefabs/PlayerPanel") as GameObject);
            playerPanels.Add(temp);
            temp.GetComponent<PlayerPerkPanel>().SetData(roundController.playerRankings[i].transform.GetComponent<PlayerManager>().playerNumber);
            temp.gameObject.transform.SetParent(playerPanel.transform, false);
        }
    }

    void GeneratePerks()
    {
        for (int i = 0; i < numberOfPerks; i++)
        {
            if (i <= 1)
            {
                perks.Add(GM.instance.lowPerks[Random.Range(0, 4)]);
            }
            else if (i <= 3)
            {
                perks.Add(GM.instance.midPerks[Random.Range(0, 4)]);
            }
            else
            {
                if(Random.Range(0,100) <= 25)
                    perks.Add(GM.instance.highPerks[Random.Range(0, 4)]);
                else
                    perks.Add(GM.instance.midPerks[Random.Range(0, 4)]);
            }
        }
    }

    void SpawnPerks()
    {
        for (int i = 0; i < numberOfPerks; i++)
        {
            //generate the perk
            GameObject temp = Instantiate(Resources.Load("PerkPrefabs/Perk") as GameObject);

            //assign the perk to the panel
            temp.AddComponent<PerkButton>();
            temp.GetComponent<PerkButton>().SetPerkName(perks[i]);
            temp.GetComponent<PerkButton>().perkChoices = gameObject.GetComponent<PerkChoices>();
            temp.GetComponent<PerkButton>().buttonNumber = i;
            //
            temp.gameObject.transform.SetParent(perkPanel.transform, false);

            perkButtons.Add(temp.GetComponent<Button>());
        }
    }

    public void RemoveButton(int number)
    {
        List<Button> temp = new List<Button>();

        foreach (Button button in perkButtons)
        {
            if (number == button.GetComponent<PerkButton>().buttonNumber)
                temp.Add(button);
        }

        perkButtons.Remove(temp[0]);
        Destroy(temp[0].gameObject);
        perkButtons[0].Select();
    }

    public void DisableButtons()
    {
        foreach(Button perk in perkButtons)
        {
            perk.interactable = false;

        }
    }

    public void EnableButtons()
    {
        foreach (Button perk in perkButtons)
        {
            perk.interactable = true;

        }
    }
}
