﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockbackCollider : MonoBehaviour
{
	public AudioClip knockbackSound;
	private AudioSource audioSource;
	private GameObject explosion;

	void Start()
	{
		audioSource = Camera.main.GetComponent<AudioSource>();
		explosion = Resources.Load("ExplosiveHit") as GameObject;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player"))
		{
			PlayerController playerController, otherPlayerController;

			otherPlayerController = col.gameObject.GetComponent<PlayerController>();
			playerController = gameObject.transform.parent.transform.parent.GetComponent<PlayerController>();

			otherPlayerController.TakeDamage(Random.Range(4, 10) + playerController.perks.addedDamage, gameObject.transform.parent.transform.parent, playerController.perks.FireChance(), playerController.perks.ColdChance(), playerController.perks.ExplodeChance());
			audioSource.PlayOneShot(knockbackSound);

			if (playerController.perks.ExplodeChance())
			{
				GameObject exp = Instantiate(explosion, transform);
				exp.GetComponent<Explosion>().AssignOwner(transform.parent.gameObject);
				exp.transform.parent = null;
			}
			else
			otherPlayerController.KnockBack(gameObject.transform.parent.position, (playerController.perks.addedKnockback + playerController.perks.moreKnockbackWhileDamaged));

        }
	}
}
