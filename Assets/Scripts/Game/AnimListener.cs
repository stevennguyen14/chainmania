﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimListener : MonoBehaviour
{
	public Collider hitBox1, hitBox2, hitBox3, kickHitBox, kickHitBox2, twoHandedHitBox, twoHandedKnockback;
	public AudioClip footStep, land, jump, throwHookSound;

	private AudioSource audioSource;

	void Start()
    {
		audioSource = Camera.main.GetComponent<AudioSource>();
    }

	public void CanCombo()
	{
		gameObject.GetComponent<PlayerController>().SetCombo(true);
	}
	//Audio 
	public void FootR()
	{
		audioSource.PlayOneShot(footStep);
	}

	public void FootL()
	{
		audioSource.PlayOneShot(footStep);
	}
 
	public void Jump()
	{
		audioSource.PlayOneShot(jump);
	}

	public void Land()
	{
		audioSource.PlayOneShot(land);
	}

     //chain stuff
	public void ThrowPlayerChain()
	{
		gameObject.GetComponent<PlayerController>().SpawnPlayerChain();
		audioSource.PlayOneShot(throwHookSound);
	}

	public void ThrowTravelChain()
	{
		gameObject.GetComponent<PlayerController>().SpawnTravelChain();
		audioSource.PlayOneShot(throwHookSound);
	}

	public void PullPlayerChain()
	{
		gameObject.GetComponent<PlayerController>().PullPlayer();
		GetComponent<PlayerController>().pullingPlayer = false;
	}

	public void PullTravelChain()
	{
		StartCoroutine(GetComponent<PlayerController>().PullTowardsWorld());
		GetComponent<PlayerController>().trigger = false;
	}

	public void PullTowardsPlayer()
	{
		//gameObject.GetComponent<PlayerController>().PullTowardsEnemy();
		StartCoroutine(GetComponent<PlayerController>().PullTowardsEnemy());
		GetComponent<PlayerController>().pullingTowardsPlayer = false;
	}

	public void SwordAbility()
	{
		GetComponent<WeaponSystem>().TwoHandedSwordAbility();
		audioSource.PlayOneShot(throwHookSound);
	}

	/*public void SpearAbility()
	{
		GetComponent<WeaponSystem>().TwoHandedSpearAbility();
	}*/

	public void AxeAbility()
	{
		GetComponent<WeaponSystem>().TwoHandedAxeAbiliy();
	}

	public void AxeSound()
	{
		GetComponent<WeaponSystem>().PlayAxeSound();
	}

	public void ShootArrow()
	{
		//shoot arrow here
		gameObject.GetComponent<WeaponSystem>().SpawnArrow();
	}

	public void ShootTrap()
	{
		//shoot arrow here
		gameObject.GetComponent<WeaponSystem>().SpawnTrap();
	}

	public void Hit1()
	{
		hitBox1.enabled = true;
	}

	public void HideHit1()
	{
		hitBox1.enabled = false;
	}

	public void Hit2()
	{
		hitBox2.enabled = true;

	}

	public void HideHit2()
	{
		hitBox2.enabled = false;
	}

	public void Hit3()
	{
		hitBox3.enabled = true;

	}

	public void HideHit3()
	{
		hitBox3.enabled = false;
	}

	public void Kick()
	{
		kickHitBox.enabled = true;

	}

	public void HideKick()
	{
		kickHitBox.enabled = false;
	}

	public void Kick2()
	{
		kickHitBox2.enabled = true;
	}

	public void HideKick2()
	{
		kickHitBox2.enabled = false;
	}

	public void TwoHandedHit()
	{
		twoHandedHitBox.enabled = true;
	}

	public void HideTwoHandedHit()
	{
		twoHandedHitBox.enabled = false;
	}

	public void TwoHandedKnock()
	{
		twoHandedKnockback.enabled = true;
	}

	public void HideTwoHandedKnock()
	{
		twoHandedKnockback.enabled = false;
	}
}
