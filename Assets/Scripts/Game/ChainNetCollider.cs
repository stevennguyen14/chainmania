﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainNetCollider : MonoBehaviour
{
	public GameObject HitEffect;
	Rigidbody rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate()
	{
		rb.AddTorque(transform.up * 20);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			other.GetComponent<PlayerController>().StunPlayer(2f);
			other.GetComponent<PlayerController>().Stun();
			//other.GetComponent<PlayerController>().TakeDamage(20, transform.position);
			Destroy(Instantiate(HitEffect, other.transform), 1f);
		}
	}
}
