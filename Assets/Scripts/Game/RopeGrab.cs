﻿using System.Collections;
using Obi;
using UnityEngine;

public class RopeGrab : MonoBehaviour
{
    public GameObject ropeOwner;
	public SphereCollider triggerCollider;
	public float hookLifeTime, grabbedLifeTime;
    public float worldGrabbedLiftTime = GameData.WORLDGRABEDLIFETIME;
    public bool grab = false;
    public bool move = false;
	public bool hooked = false;
	public bool attached = false;
    Rigidbody rigi;
    public int ropeNumber;
	public AudioClip hookGroundSound, hookPlayerSound;

	//rope physics
	public GameObject ropeRed, ropeGreen, ropeOrange, ropeBlue;
	public float speed = 30f;
	public float distance;
	bool done = false;

	//private
	private GameObject otherPlayer;
	private GameObject instantiatedRope;
	private bool connected = false;
	private ObiRope obiRope;
	private float timeLeft;
	private bool triggered = false;
	private AudioSource audioSource;

    GameObject pickedUpWeapon;
    bool pullingWeapon;

    // Start is called before the first frame update
    void Start()
    {
		rigi = GetComponent<Rigidbody>();
		audioSource = Camera.main.GetComponent<AudioSource>();
		if (move)
			rigi.mass = 1;

		//instantiate an obi rope gameObject and assign the appropriate color
		if(ChainColor() == 1)
			instantiatedRope = Instantiate(ropeOrange, transform.position, Quaternion.identity);
		else if (ChainColor() == 2)
			instantiatedRope = Instantiate(ropeRed, transform.position, Quaternion.identity);
		else if (ChainColor() == 3)
			instantiatedRope = Instantiate(ropeGreen, transform.position, Quaternion.identity);
		else if (ChainColor() == 4)
			instantiatedRope = Instantiate(ropeBlue, transform.position, Quaternion.identity);

		//assign the obi rope 
		obiRope = instantiatedRope.GetComponent<ObiRope>();
        //get the Obi Particle Handle from the rope owner and assign the actor to this instantiated obi rope
        if(ropeNumber == 1)
            ropeOwner.GetComponent<PlayerController>().GetObiHandle().GetComponent<ObiParticleHandle>().Actor = obiRope;
        if(ropeNumber == 2)
            ropeOwner.GetComponent<PlayerController>().GetObiHandle2().GetComponent<ObiParticleHandle>().Actor = obiRope;
        if (ropeNumber == 3)
            ropeOwner.GetComponent<PlayerController>().GetObiHandle3().GetComponent<ObiParticleHandle>().Actor = obiRope;
        //assign the hook to the pin constraint
        AssignPinConstraint(gameObject.GetComponent<ObiCollider>(), new Vector3(0, 0, 0));
		timeLeft = hookLifeTime;
	}

	private void Update()
	{
		timeLeft -= Time.deltaTime;
		if(timeLeft < 0)
		{
			RemovePinConstraint();
			Destroy(instantiatedRope);
			ResetThrowingBool();
			ropeOwner.GetComponent<PlayerController>().trigger = false;
			ropeOwner.GetComponent<PlayerController>().dragonHead.SetActive(false);
			ropeOwner.GetComponent<AnimListener>().HideTwoHandedKnock();
			Destroy(gameObject);
		}
	}

    public void DestroyRope()
    {
        StartCoroutine(DestroyRopes());
    }

    IEnumerator DestroyRopes()
    {
        RemovePinConstraint();
        yield return new WaitForSeconds(1);
        Destroy(instantiatedRope);
        ResetThrowingBool();
        Destroy(gameObject);
    }

    void PullWeapon()
    {
        pickedUpWeapon.transform.position = Vector3.MoveTowards(pickedUpWeapon.transform.position, transform.position, Time.deltaTime * 10);
    }

    void AssignPinConstraint(ObiCollider obi, Vector3 attachmentOffset)
	{
		// simply add a new pin constraint
		obiRope.PinConstraints.RemoveFromSolver(null);
		ObiPinConstraintBatch batch = (ObiPinConstraintBatch)obiRope.PinConstraints.GetFirstBatch();
		batch.AddConstraint(obiRope.UsedParticles - 1, obi, attachmentOffset, Quaternion.identity, 1);
		obiRope.PinConstraints.AddToSolver(null);
	}

	void RemovePinConstraint()
	{
        // simply remove all pin constraints
		obiRope.PinConstraints.RemoveFromSolver(null);
		obiRope.PinConstraints.GetFirstBatch().Clear();
		obiRope.PinConstraints.AddToSolver(null);
    }

	public void SetMove()
    {
        move = true;
    }

    public void SetGrab()
    {
        grab = true;
    }

    public void SetOwner(GameObject owner)
    {
        ropeOwner = owner;
    }

	void ResetThrowingBool()
	{
		ropeOwner.GetComponent<PlayerController>().throwing = false;
		ropeOwner.GetComponent<PlayerController>().throwingTravel = false;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (grab)
        {
            if (other.tag == "PlayerRopeBox")
			{
				//camera shake
				StartCoroutine(Camera.main.GetComponent<CameraShaker>().CameraShake(0.4f));
				audioSource.PlayOneShot(hookPlayerSound);
				ropeOwner.GetComponent<PlayerController>().AttachRope(other.transform.parent.GetComponent<Collider>());

				//assign the other player
				AssignOtherPlayer(other.gameObject);

				//remove hook with the pin constraint
				RemovePinConstraint();
				//assign the other player to the pin constraint
				AssignPinConstraint(otherPlayer.GetComponent<ObiCollider>(), new Vector3(0, 1, 0));
				//set other player obi rigidbody kinematic to false, so they can be affected by the rope tension
				otherPlayer.GetComponent<ObiRigidbody>().kinematicForParticles = false;
				//make the rope tearable
				//obiRope.tearable = true;
				
				gameObject.transform.Find("SwordTrail").gameObject.SetActive(false);
				rigi.isKinematic = true;
				triggerCollider.enabled = false;
				GetComponent<SphereCollider>().enabled = false;
				GetComponent<MeshRenderer>().enabled = false;

				if (ropeOwner.GetComponent<PlayerController>().pullingPlayer)
				{
					ropeOwner.GetComponent<PlayerController>().GetAnimator().SetTrigger("pullPlayerChain");
				}
				else if (ropeOwner.GetComponent<PlayerController>().pullingTowardsPlayer)
				{
					ropeOwner.GetComponent<PlayerController>().GetAnimator().SetTrigger("pullTowardsPlayer");
				}

				//reset the timer to grabbed timer
				timeLeft = 1f;
			}

            if (other.tag == "Bow" || other.tag == "TwoHandedSword" || other.tag == "TwoHandedSpear")
            {
                print("Hit weapon");
                pickedUpWeapon = other.gameObject;
                RemovePinConstraint();
                AssignPinConstraint(pickedUpWeapon.GetComponent<ObiCollider>(), new Vector3(0, 0, 0));
                pickedUpWeapon.GetComponent<ObiRigidbody>().kinematicForParticles = false;
                //make the rope tearable
                obiRope.tearable = true;
                gameObject.transform.Find("SwordTrail").gameObject.SetActive(false);
                rigi.isKinematic = true;
                triggerCollider.enabled = false;
                GetComponent<SphereCollider>().enabled = false;
                GetComponent<MeshRenderer>().enabled = false;
            }
        }

        if (move)
        {
            if (other.tag == "World")
            {
				//make sure it is run only once
				if (!triggered)
				{
					rigi.isKinematic = true;
					audioSource.PlayOneShot(hookGroundSound);
					ropeOwner.GetComponent<PlayerController>().AttachWorld();
					//pulling is run through the animation event calling the function in the Anim Listener
					ropeOwner.GetComponent<PlayerController>().GetAnimator().SetTrigger("pullTravelChain");
					timeLeft = worldGrabbedLiftTime;
					triggered = true;
				}
			}
        }       
    }

	int? ChainColor()
	{
		if (ropeOwner.name == "Player 1")
		{
			return 1;
		}
		if (ropeOwner.name == "Player 2")
		{
			return 2;
		}
		if (ropeOwner.name == "Player 3")
		{
			return 3;
		}
		if (ropeOwner.name == "Player 4")
		{
			return 4;
		}
		else return null;
	}

	void AssignOtherPlayer(GameObject enemy)
	{
		otherPlayer = enemy.transform.parent.gameObject;
	}

    public void SetRopeNumber(int number)
    {
        ropeNumber = number;
    }

}