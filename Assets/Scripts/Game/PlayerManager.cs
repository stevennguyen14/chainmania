﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerManager : MonoBehaviour
{
    Perks perks;
    public PlayerController playerController;

    public GameObject player;

    //FX
    public ParticleManager particleManager;
    public RoundController roundController;

    public int points;

    //Player Stats
    public int lives;
    public int health;
    public int maxHealth;
    public int playerNumber;

    //player UI
    public GameObject healthText;
    public Text livesText;
    public List<GameObject> livesImages;

    //spawn protection
    public bool spawned;

    //playerUI panel
    public GameObject uiPanel;
    public GameObject canvas;

    //player color
    private Color blue = Color.blue;
    private Color red = Color.red;
    private Color green = Color.green;
    private Color purple = Color.magenta;

    private void Awake()
    {
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();

        SetPlayerNumber();
        playerController.SetPlayerColour(playerNumber);
        perks = gameObject.GetComponent<Perks>();
        canvas = GameObject.Find("Canvas");
    }

    //move later
    public void CreateUiPanel()
    {
        if (GM.instance != null)
        {
            uiPanel = Instantiate(Resources.Load("UIPrefabs/Player UI Box") as GameObject);
            uiPanel.gameObject.transform.SetParent(canvas.transform.Find("PlayerPanels"), false);

            //set the text mesh pro name
            GameObject playerName = uiPanel.gameObject.transform.Find("BG/Player Name").gameObject;
            playerName.GetComponent<TextMeshProUGUI>().text = "Player " + playerNumber;

            //set the player health
            healthText = uiPanel.gameObject.transform.Find("BG/Health").gameObject;

            //set ui panel color
            GameObject BG = uiPanel.gameObject.transform.Find("BG").gameObject;
            if (playerNumber == 1) BG.GetComponent<Image>().color = purple;
            else if (playerNumber == 2) BG.GetComponent<Image>().color = red;
            else if (playerNumber == 3) BG.GetComponent<Image>().color = green;
            else if (playerNumber == 4) BG.GetComponent<Image>().color = blue;


            healthText.GetComponent<TextMeshProUGUI>().text = maxHealth.ToString();

            //set the player lives
            for (int i = 0; i < lives; i++)
            {
                //create lives icon
                GameObject temp = Instantiate(Resources.Load("UIPrefabs/PlayerLifeIcon") as GameObject);

                //add that image to a list to remove later
                livesImages.Add(temp);

                temp.gameObject.transform.SetParent(uiPanel.gameObject.transform.Find("BG/Lives Panel"), false);
            }
        }
    }

    public void GetUi()
    {
        if (GM.instance != null)
        {
            string healthFinder = "Player " + playerNumber + " Health";
            string livesFinder = "Player " + playerNumber + " Lives Number";
        }
    }

    public void UpdateHealth()
    {
        if (GM.instance != null) healthText.GetComponent<TextMeshProUGUI>().text = health.ToString();
    }

    public void UpdateLives()
    {
        if (GM.instance != null)
        {
            livesImages[livesImages.Count - 1].gameObject.SetActive(false);
            livesImages.RemoveAt(livesImages.Count - 1);
        }
    }


    public bool spawnProtectionOn = false;

    public void SetUpPlayer()
    {
        health = GameData.BASEHEALTH;
        lives = 2;

        SetPlayerNumber();

        //set up player UI Panel
        GetUi();
        CreateUiPanel();
        UpdateHealth();
        perks.TurnOffParticles();
        perks.GetPerks();
        ManagePerks();
        playerController.SpawnPlayers();
    }

    public void SetPlayerNumber()
    {
        if (gameObject.name == "Player 1 Manager") playerNumber = 1;
        else if (gameObject.name == "Player 2 Manager") playerNumber = 2;
        else if (gameObject.name == "Player 3 Manager") playerNumber = 3;
        else if (gameObject.name == "Player 4 Manager") playerNumber = 4;
    }

    public void Death()
    {

        if (perks.FreeRevive())
        {
            //ADD BIG FUCKOFF SOUND HERE FOR REVIVE
            health = GameData.BASEHEALTH / 2;
            print(playerNumber + " Has gotten a free revive!");
            particleManager.TurnOnAngelWings();
        }
        else
        {
            lives--;
            UpdateLives();
        }

        playerController.jump = false;
        health = GameData.BASEHEALTH;
        UpdateHealth();

        roundController.GetLivingPlayers();
        player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        if (lives <= 0)
        {
            //add player to rankings
            roundController.playerRankings.Add(gameObject);
            gameObject.SetActive(false);
            //set UI to dead
            Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();
            Destroy(uiPanel);
        }
    }

    public void AddScore(int addedScore)
    {
        points += addedScore;
    }

    public void RoundStartSpawn()
    {
        if (playerNumber == 1)
            transform.position = roundController.spawnPoints[0].transform.position;
        else if (playerNumber == 2)
            transform.position = roundController.spawnPoints[1].transform.position;
        else if (playerNumber == 3)
            transform.position = roundController.spawnPoints[2].transform.position;
        else if (playerNumber == 4)
            transform.position = roundController.spawnPoints[3].transform.position;
    }

    public void ManagePerks()
    {
        perks.ResetPerks();
        perks.SetPerks();
    }
}
