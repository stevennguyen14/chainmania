﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MultipleTargetCamera : MonoBehaviour {

	public GameObject[] targets;
	public Vector3 offset = new Vector3(0, 0, -37f);
	public float smoothTime = 0.35f;
	public float minZoom = 60f;
	public float maxZoom = 20f;
	public float zoomLimiter = 50f;

	private Vector3 velocity;
	private Camera cam;

	[SerializeField]
    private GameObject[] players;

    // Use this for initialization
    void Start () {
		cam = Camera.main.GetComponent<Camera>();
		UpdateTargets();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (targets.Length == 0) {
			return;
		}

		Move ();
		Zoom ();

	}

	public void UpdateTargets()
	{
		targets = GameObject.FindGameObjectsWithTag("Player");
	}

	Vector3 GetCenterPoint(){
		if (targets.Length == 1) {
			return targets [0].transform.position;
		}

		var bounds = new Bounds (targets [0].transform.position, Vector3.zero);
		for (int i = 0; i < targets.Length; i++) {
			bounds.Encapsulate (targets [i].transform.position);
		}

		return bounds.center;
	}

	float GetGreatestDistance(){

		var bounds = new Bounds (targets [0].transform.position, Vector3.zero);
		for (int i = 0; i < targets.Length; i++) {
			bounds.Encapsulate (targets [i].transform.position);
		}

		float diagonalBounds = (Mathf.Pow (bounds.size.x, 2) + Mathf.Pow (bounds.size.z, 2));
		diagonalBounds = Mathf.Sqrt (diagonalBounds);

		return  diagonalBounds;

	}

	void Zoom(){

		float newZoom = Mathf.Lerp (maxZoom, minZoom, GetGreatestDistance () / zoomLimiter);
		cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
	}

	void Move(){
		
		Vector3 centerPoint = GetCenterPoint ();

		Vector3 newPosition = centerPoint + offset;

		transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
	}
}
