﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxCollider : MonoBehaviour
{

	public AudioClip[] hit;

	private AudioSource audioSource;

	void Start()
	{
		audioSource = Camera.main.GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Player"))
		{
			PlayerController playerController, otherPlayerController;

			otherPlayerController = col.gameObject.GetComponent<PlayerController>();
			playerController = gameObject.transform.parent.GetComponent<PlayerController>();

			otherPlayerController.TakeDamage(Random.Range(2, 6) + playerController.perks.addedDamage, this.transform.parent, playerController.perks.FireChance(), playerController.perks.ColdChance(), playerController.perks.ExplodeChance());
			int hit = Random.Range(0, 2);
			audioSource.PlayOneShot(this.hit[hit]);
		}
	}
}
