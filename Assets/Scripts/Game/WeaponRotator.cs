﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRotator : MonoBehaviour
{
	int rotateRate = 1;

	// Update is called once per frame
	void FixedUpdate()
    {
		transform.localRotation = Quaternion.Euler(-85, rotateRate++ , 0);
	}
}
