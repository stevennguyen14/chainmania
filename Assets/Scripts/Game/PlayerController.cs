﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Game Controllers
    public Perks perks;
    public PlayerManager playerManager;
    public ParticleManager particleManager;
    public RoundController roundController;

    //public
    public float speed = GameData.SPEED;
	public float strafeSpeed = GameData.STRAFESPEED;
	public float gravity = GameData.GRAVITY;
	public float maxVelocityChange = GameData.MAXVELOCITYCHANGE;
	public float jumpHeight = GameData.JUMPHEIGHT;
	public float rotationLerpSpeed = GameData.ROTATIONLERPSPEED;
    public float knockbackForce = GameData.KNOCKBACKFORCE;
    public float initialKnockbackForce = GameData.INITIALKNOCKBACKFORCE;
	public float grappleForce= GameData.GRAPPLEFORCE;
	public float upForce = GameData.UPFORCE;

	public GameObject hitParticle, hitParticle2, dragonParticle, dragonHead;
	public bool trigger = false;
	public bool jump = false;
	public Rigidbody rb;

	//private
	private float defaultSpeed;
	private bool grounded = false;
	private Animator anim;
	private Quaternion rotateTo;
	private float hori, verti, hori2, verti2;
	private bool running;
	private bool strafing;
	private bool stunned;
	private bool died;
	private bool looking;
	private bool moving;
	private GameObject deathParticle;
	private Vector3 lastHitDirection;

	[SerializeField]
	private float timeLeft;
	Vector3 velocityChange;

	[SerializeField]
	private string horiString;
	[SerializeField]
	private string vertiString;
	[SerializeField]
	private string lookHoriString;
	[SerializeField]
	private string lookVertiString;
	[SerializeField]
	private string jumpString;
	public string xString;
	public string yString;
	public string bString;
	[SerializeField]
	private string rightButtonString;
	[SerializeField]
	private string leftButtonString;
	public string leftTriggerString;
	[SerializeField]
	private string rightTriggerString;
    [SerializeField]
    private string startButton;

	//------------
	//PLAYER LINKS
	public GameObject childs;

	//Textures <---- Move out later or change to array
	public Renderer rend;
	public Texture blueTexture;
	public Texture redTexture;
	public Texture greenTexture;
	public Texture purpleTexture;

	//Direction UI
	public Sprite pointer1, pointer2, pointer3, pointer4;
	public SpriteRenderer pointerRenderer;

	//Chain Stuff
	public Transform chainRoot;
	public GameObject enemy;
	public GameObject ropeConector;

    //changed to list so we can throw multiple
    public List<GameObject> currentRope;

	GameObject currentTravelRope;
	public GameObject obiHandle;
    public GameObject obiHandle2;
    public GameObject obiHandle3;

    public Collider hitboxCol;

    bool hasInit = false;

    void OnEnable()
	{
        if(!hasInit)
            Init();

        childs.SetActive(true);
		died = false;
		throwingTravel = false;
		trigger = false;
        throwing = false;
        running = false;
		strafing = false;
		stunned = false;
		anim.SetBool("canCombo", true);
		anim.SetBool("death", false);
	}

    public void Init()
    {
        hasInit = true;
        if (GM.instance.gameMode == GameData.DEATHMATCH)
            roundController = GameObject.Find("RoundController").GetComponent<RoundController>();

        deathParticle = Resources.Load("DeathParticle") as GameObject;
        particleManager = gameObject.GetComponent<ParticleManager>();

        rb = gameObject.GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        rb.useGravity = false;
        died = false;
        looking = false;
        anim = gameObject.GetComponent<Animator>();
        defaultSpeed = GameData.SPEED;

        playerManager = transform.parent.GetComponent<PlayerManager>();
        playerManager.SetPlayerNumber();

        //set strings for controlls uniques to each player number
        horiString = "HorizontalP" + playerManager.playerNumber;
        vertiString = "VerticalP" + playerManager.playerNumber;
        lookHoriString = "LookHoriP" + playerManager.playerNumber;
        lookVertiString = "LookVertiP" + playerManager.playerNumber;
        jumpString = "JumpP" + playerManager.playerNumber;
        xString = "Attack1P" + playerManager.playerNumber;
        yString = "Attack2P" + playerManager.playerNumber;
        bString = "ThrowP" + playerManager.playerNumber;
        rightButtonString = "RightButtonP" + playerManager.playerNumber;
        leftButtonString = "LeftButtonP" + playerManager.playerNumber;
        leftTriggerString = "LeftTriggerP" + playerManager.playerNumber;
        rightTriggerString = "RightTriggerP" + playerManager.playerNumber;
        startButton = "StartButtonP" + playerManager.playerNumber;

        roundController = GameObject.Find("RoundController").GetComponent<RoundManagerDeathmatch>();
    }

    float leftTrigger;
    float rightTrigger;

	void FixedUpdate()
	{
        if (!roundController.countingDown)
        {                

            if (!roundController.roundPaused)
            {

                // Calculate how fast we should be moving
                Vector3 targetVelocity = new Vector3(hori, 0, verti);
                if (targetVelocity != Vector3.zero)
                    rotateTo = Quaternion.LookRotation(targetVelocity);
                targetVelocity *= speed;

                //look at joystick rotation
                if (looking)
                {
                    strafing = true;
                    anim.SetBool("strafing", true);
                    speed = strafeSpeed + perks.addedSpeed;
                    Quaternion rotateTo2 = Quaternion.LookRotation(new Vector3(hori2, 0, -verti2));
                    transform.rotation = Quaternion.Lerp(transform.rotation, rotateTo2, Time.deltaTime * rotationLerpSpeed);
                }
                else
                {
                    strafing = false;
                    anim.SetBool("strafing", false);
                    speed = defaultSpeed + perks.addedSpeed;
                }

                if (grounded)
                {
                    //falling is false
                    anim.SetBool("falling", false);

                    // Apply a force that attempts to reach our target velocity
                    Vector3 velocity = rb.velocity;
                    anim.SetFloat("velocity", velocity.magnitude);
                    velocityChange = (targetVelocity - velocity);
                    velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
                    velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
                    velocityChange.y = 0;

                    rb.AddForce(velocityChange, ForceMode.VelocityChange);

                    //use this to call jump from other scripts
                    if (jump)
                    {
                        rb.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
                        anim.SetBool("jump", true);
                    }
                }
                else
                {
                    anim.SetBool("falling", true);

					// Apply a force that attempts to reach our target velocity
					Vector3 velocity = rb.velocity;
					velocityChange = (targetVelocity/2f - velocity);
					velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
					velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
					velocityChange.y = 0;

					if (targetVelocity != Vector3.zero)
						rb.AddForce(velocityChange, ForceMode.VelocityChange);
				}

                // We apply gravity manually for more tuning control
                rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));
                grounded = false;
            }
        }       
	}

    public bool amIpicking;
    public bool perkMenuOpen = false;

	void Update()
	{
        if (!roundController.countingDown)
        {
            if (Input.GetButton(startButton) && !roundController.roundPaused && !roundController.pickingPerks)
            {
                roundController.PauseRound(playerManager.playerNumber);
                print(playerManager.playerNumber + " Has pressed start!");
            }

			if(!roundController.roundPaused)
			{
				//cancel spawn protection
				if (playerManager.spawnProtectionOn && (Input.GetAxisRaw(horiString) != 0 || Input.GetAxisRaw(vertiString) != 0 || Input.GetAxisRaw(lookHoriString) != 0 || Input.GetAxisRaw(lookVertiString) != 0))
                    CancelSpawnProtection();
				else if (playerManager.spawnProtectionOn && (Input.GetButton(jumpString) || Input.GetButton(xString) || Input.GetButton(yString) || Input.GetAxisRaw(leftTriggerString) != 0 || Input.GetAxisRaw(rightTriggerString) != 0))
                    CancelSpawnProtection();

				hori = Input.GetAxisRaw(horiString);
				verti = Input.GetAxisRaw(vertiString);

				hori2 = Input.GetAxisRaw(lookHoriString);
				verti2 = Input.GetAxisRaw(lookVertiString);

				//trigger inputs
				leftTrigger = Input.GetAxisRaw(leftTriggerString);
				rightTrigger = Input.GetAxisRaw(rightTriggerString);

				//look at joystick rotation
				if ((hori2 > 0 || hori2 < 0 || verti2 > 0 || verti2 < 0) && !stunned)
				{
					looking = true;
				}
				else
				{
					looking = false;
				}

				//rotate player to movement direction
				if ((hori > 0 || hori < 0 || verti > 0 || verti < 0) && !stunned)
				{
					if (!strafing)
					{
						transform.rotation = Quaternion.Lerp(transform.rotation, rotateTo, Time.deltaTime * rotationLerpSpeed);
					}
					running = true;
				}
				else
				{
					running = false;
				}

				if ((Input.GetButton(jumpString) && !stunned))
				{
					jump = true;
				}
				else
				{
					jump = false;
				}

				// Attacking();
				ThrowingRope();
				MovementAnimation();
			}


            if (roundController.pickingPerks)
            {
                if (roundController.perkPickingPlayer == playerManager.playerNumber)
                    amIpicking = true;
                else
                    amIpicking = false;

                if (Input.GetButtonDown(startButton) && amIpicking && !perkMenuOpen)
                {
                    roundController.perkPreview.SetActive(true);
                    perkMenuOpen = true;
                }

                if (Input.GetButtonDown(bString) && amIpicking && perkMenuOpen)
                {
                    roundController.perkPreview.SetActive(false);
                    perkMenuOpen = false;
                }
            }

			//getting stunned
            if (stunned)
            {
                speed = 0;
                timeLeft -= Time.deltaTime;
                if (anim.GetBool("stunned") == false)
                {
                    anim.SetBool("stunned", true);
                }
                if (timeLeft < 0)
                {
                    anim.SetBool("stunned", false);
                    speed = defaultSpeed + perks.addedSpeed;
                    stunned = false;
                }
            }

			//die if health equals zero
			if(playerManager.health <= 0 && !died)
			{
				died = true;
                playerManager.health = 0;
				//set animation
				Destroy(Instantiate(deathParticle, transform.position + new Vector3(0,2,0), Quaternion.FromToRotation(Vector3.forward, lastHitDirection)), 2);
				childs.SetActive(false);
				anim.SetBool("death", true);
				//call deathdelay function
				StartCoroutine(DeathDelay());
			}

			//do this for now, so health can't be changed when hit and player is dead
			if (died)
			{
				playerManager.health = 0;
                playerManager.UpdateHealth();
			}
        }
	}

	IEnumerator DeathDelay()
	{
		yield return new WaitForSeconds(2f);
		playerManager.Death();
		//if player still have lives
		if (playerManager.lives > 0)
		{
			//get reference to deathbox script
			DeathBox deathBox = GameObject.Find("DeathBox").GetComponent<DeathBox>();
			//start the spawn delay
			deathBox.StartSpawnDelayCoroutine(this.gameObject, 2);
			//temp deactivate player 
			gameObject.SetActive(false);
			//reset camera 
			Camera.main.transform.parent.GetComponent<MultipleTargetCamera>().UpdateTargets();
            perks.TurnOffParticles();
		}
		died = false;
	}

	void OnCollisionStay()
	{
		if (headVisible)
		{
			dragonHead.SetActive(false);
			GetComponent<AnimListener>().HideTwoHandedKnock();
			headVisible = false;
		}
		grounded = true;
		anim.SetBool("jump", false);
	}

	float CalculateJumpVerticalSpeed()
	{
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}

	void MovementAnimation()
	{
		Vector3 transformedDirection = transform.TransformDirection(rb.velocity);

		if (System.Math.Round(transformedDirection.magnitude, 4) == System.Math.Round(rb.velocity.magnitude, 4))
		{
			//float anim parameter for running animation blend
			anim.SetFloat("runX", transformedDirection.x);
			anim.SetFloat("runY", transformedDirection.z);
		}
		else
		{
			//float anim parameter for running animation blend
			anim.SetFloat("runX", -transformedDirection.x);
			anim.SetFloat("runY", -transformedDirection.z);
		}

		//set animation
		if (hori != 0 || verti != 0)
		{
			anim.SetBool("run", true);
		}
		else
		{
			anim.SetBool("run", false);
		}
	}

	public void SetCombo(bool boolean)
	{
		anim.SetBool("canCombo", boolean);
	}

	/*void Attacking()
	{
		//attack combo
		if (!gameObject.GetComponent<WeaponSystem>().bow.activeSelf && Input.GetButtonDown(xString) || Input.GetMouseButtonDown(0))
		{
			if (!stunned)
			{
				if (anim.GetBool("canCombo"))
				{
					int combo = anim.GetInteger("combo");

					combo++;

					anim.SetInteger("combo", combo);

					SetCombo(false);
				}
			}
		}

		if (!gameObject.GetComponent<WeaponSystem>().bow.activeSelf && Input.GetButtonDown(yString) && !stunned || Input.GetMouseButtonDown(1))
		{

			int combo2 = anim.GetInteger("combo2");

			combo2++;

			anim.SetInteger("combo2", combo2);
		}

		//if player has a bow
		if (gameObject.GetComponent<WeaponSystem>().bow.activeSelf && rightTrigger < -0.1f && ropedPlayer.Count == 0)
		{

			int combo = anim.GetInteger("combo");

			combo++;

			anim.SetInteger("combo", combo);
		}

		//if the player has a bow 
		if (gameObject.GetComponent<WeaponSystem>().bow.activeSelf && Input.GetButtonDown(leftButtonString) && !ropeGrounded)
		{
			int combo2 = anim.GetInteger("combo2");

			combo2++;

			anim.SetInteger("combo2", combo2);
		}

	}*/


	//Rope Throwing stuff
	public List<GameObject> ropedPlayer = null;
	public bool throwing = false;
	public bool pullingPlayer = false;
	public bool pullingTowardsPlayer = false;
	public bool throwingTravel = false;
    GameObject thrownRope;
    GameObject secondThrownRope;
    GameObject thirdThrownRope;

    public void SpawnPlayerChain()
	{
		throwing = true;
		thrownRope = Instantiate(ropeConector, ((transform.position + new Vector3(0, 2.6f, 0)) + transform.forward * 1.5f), (transform.rotation * Quaternion.Euler(90, 0, 0)));
		thrownRope.GetComponent<Rigidbody>().AddForce(transform.forward * 1000f);
		thrownRope.GetComponent<RopeGrab>().SetOwner(this.gameObject);
		thrownRope.GetComponent<RopeGrab>().SetGrab();
        thrownRope.GetComponent<RopeGrab>().SetRopeNumber(1);
        
        if (gameObject.GetComponent<WeaponSystem>().holdingBow)
        {
            print("Shooting 2 other chains"); 
            secondThrownRope = Instantiate(ropeConector, ((transform.position + new Vector3(2, 2.6f, 2)) + transform.forward * 1.5f), (transform.rotation * Quaternion.Euler(90, 5, 0)));
            secondThrownRope.GetComponent<Rigidbody>().AddForce(transform.forward * 1000f);
            secondThrownRope.GetComponent<RopeGrab>().SetOwner(this.gameObject);
            secondThrownRope.GetComponent<RopeGrab>().SetGrab();
            secondThrownRope.GetComponent<RopeGrab>().SetRopeNumber(2);

            thirdThrownRope = Instantiate(ropeConector, ((transform.position + new Vector3(-2, 2.6f, -2)) + transform.forward * 1.5f), (transform.rotation * Quaternion.Euler(90, -5, 0)));
            thirdThrownRope.GetComponent<Rigidbody>().AddForce(transform.forward * 1000f);
            thirdThrownRope.GetComponent<RopeGrab>().SetOwner(this.gameObject);
            thirdThrownRope.GetComponent<RopeGrab>().SetGrab();
            thirdThrownRope.GetComponent<RopeGrab>().SetRopeNumber(3);
        }
        
	}

	public void SpawnTravelChain()
	{
		throwingTravel = true;
		GameObject thrownTravelRope = Instantiate(ropeConector, ((transform.position + new Vector3(0, 1.6f, 0)) + transform.forward * 1.5f), (transform.rotation * Quaternion.Euler(90, 0, 0)));
		thrownTravelRope.GetComponent<Rigidbody>().AddForce(transform.forward * 700f);
		thrownTravelRope.GetComponent<RopeGrab>().SetOwner(this.gameObject);
		thrownTravelRope.GetComponent<RopeGrab>().SetMove();
        thrownTravelRope.GetComponent<RopeGrab>().SetRopeNumber(3);
        currentTravelRope = thrownTravelRope;
	}

	void ThrowingRope()
	{

		/*if (Input.GetButtonDown(pullPlayerString) && !throwing)
		{
			anim.SetTrigger("throwPlayerChain");
			pullingPlayer = true;
		}*/

		/*else if(!gameObject.GetComponent<WeaponSystem>().bow.activeSelf && Input.GetButtonDown(pullTowardsPlayerString) && !throwing)
		{
			anim.SetTrigger("throwPlayerChain");
			pullingTowardsPlayer = true;
		}*/

		//if movement rope is thrown
		if (rightTrigger < -0.8f && !trigger && !throwingTravel && !throwing)
		{
			anim.SetTrigger("throwTravelChain");
			trigger = true;
		}
	}

	public GameObject GetObiHandle()
	{
		return obiHandle;
	}

    public GameObject GetObiHandle2()
    {
        return obiHandle2;
    }

    public GameObject GetObiHandle3()
    {
        return obiHandle3;
    }

    public void AttachRope(Collider otherPlayer)
	{
		if (otherPlayer.gameObject.name != "Player " + playerManager.playerNumber)
		{
            if(!ropedPlayer.Contains(otherPlayer.gameObject))
			    ropedPlayer.Add(otherPlayer.transform.gameObject);
		}
	}

	public void AttachWorld()
	{
		throwing = false;
	}

    public void PullPlayer()
	{
        foreach (GameObject player in ropedPlayer)
        {
            player.GetComponent<PlayerController>().GetPulled(this.gameObject);
        }

        if(thrownRope != null)
            thrownRope.GetComponent<RopeGrab>().DestroyRope();
        if (secondThrownRope != null)
            secondThrownRope.GetComponent<RopeGrab>().DestroyRope();
        if (thirdThrownRope != null)
            thirdThrownRope.GetComponent<RopeGrab>().DestroyRope();

        ropedPlayer.Clear();
    }

	public void GetPulled(GameObject playerPulledTo)
	{
		Vector3 oppDirection = playerPulledTo.transform.position - transform.position;
		oppDirection = oppDirection.normalized;
		rb.AddForce((oppDirection + (transform.up * 0.3f)) * grappleForce, ForceMode.Impulse);
	}

	public IEnumerator PullTowardsEnemy()
	{
		Vector3 oppDirection = ropedPlayer[0].transform.position - transform.position;
		oppDirection.y = 0;
		float dist = oppDirection.magnitude;

		bool velocityResetted = false;
		bool kicked = false;
		while (dist > 2f)
		{
			//Add force again
			oppDirection = ropedPlayer[0].transform.position - transform.position;
			rb.AddForce((oppDirection.normalized + (transform.up * 0.3f)) * 15f, ForceMode.Impulse);

			//calculate the distance again
			oppDirection.y = 0;
			dist = oppDirection.magnitude;
			//look at player
			Quaternion rotation = Quaternion.LookRotation(oppDirection);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);

			if(dist < 6f && !kicked)
			{
				anim.SetTrigger("chainKick");
				kicked = true;
			}

			//once reached the player, reset the velocity 
			if (dist < 2.6f && !velocityResetted)
			{
				rb.velocity = Vector3.zero;
				velocityResetted = true;
			}

			yield return new WaitForFixedUpdate();
		}
		
    }

	public bool headVisible = false;

	IEnumerator DragonHeadBoolDelay()
	{
		yield return new WaitForSeconds(1f);
		headVisible = true;
	}

	public IEnumerator PullTowardsWorld()
	{
		Vector3 oppDirection = currentTravelRope.transform.position - transform.position;
		oppDirection.y = 0;
		float dist = oppDirection.magnitude;

		dragonHead.SetActive(true);
		GetComponent<AnimListener>().TwoHandedKnock();
		StartCoroutine(DragonHeadBoolDelay());

		while (dist > 1f && currentTravelRope != null)
		{
			//calculate direction
			oppDirection = currentTravelRope.transform.position - transform.position;
			//addforce towards direciton
			rb.AddForce((oppDirection.normalized + (transform.up * 0.3f)) * 20f, ForceMode.Impulse);
			//reset oppDirection y
			oppDirection.y = 0f;
			//rotate player towards direction
			Quaternion rotation = Quaternion.LookRotation(oppDirection);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);
			//get distance
			dist = Vector3.Distance(currentTravelRope.transform.position, transform.position);

			//braking after reaching a certain distance
			if (dist < 6f)
			{
				rb.velocity = rb.velocity * 0.85f;
			}

			yield return new WaitForFixedUpdate();
		}
	}

	public void StunPlayer(float time)
	{
		stunned = true;
		timeLeft = time;
	}

	public void AssignPlayer(int playerNum)
	{
        playerManager.playerNumber = playerNum;
	}

	public void Stun()
	{
		anim.SetTrigger("Hit");
	}

    public void KnockBack(Vector3 otherPlayer, float moreKnockback)
	{
		//knock back here based on health
		//knockback in opposite direction
		Vector3 oppDirection = otherPlayer - transform.position;
		oppDirection.y = 0;
		oppDirection = -oppDirection.normalized;

		//tweak this formula please knockback resistance
		rb.AddForce((oppDirection + (transform.up * upForce)) * ((knockbackForce * 1 + moreKnockback) - perks.lessKnockback), ForceMode.Impulse);

		//camera shake
		StartCoroutine(Camera.main.GetComponent<CameraShaker>().CameraShake(0.8f));
	}

	//overload
	public void KnockBack(Vector3 otherPlayer, float moreKnockback, float knockbackForce)
	{
		//knock back here based on health
		//knockback in opposite direction
		Vector3 oppDirection = otherPlayer - transform.position;
		oppDirection.y = 0;
		oppDirection = -oppDirection.normalized;

		//tweak this formula please knockback resistance
		rb.AddForce((oppDirection + (transform.up * upForce)) * ((knockbackForce * 1 + moreKnockback) - perks.lessKnockback), ForceMode.Impulse);

		//camera shake
		StartCoroutine(Camera.main.GetComponent<CameraShaker>().CameraShake(0.8f));
	}

    public Animator GetAnimator()
	{
		return anim;
	}

    public void SetPlayerColour(int number)
    {
        if (gameObject.name == "Player 1")
        {
            rend.material.mainTexture = purpleTexture;
            pointerRenderer.sprite = pointer1;
        }
        if (gameObject.name == "Player 2")
        {
            rend.material.mainTexture = redTexture;
            pointerRenderer.sprite = pointer2;
        }
        if (gameObject.name == "Player 3")
        {
            rend.material.mainTexture = greenTexture;
            pointerRenderer.sprite = pointer3;
        }
        if (gameObject.name == "Player 4")
        {
            rend.material.mainTexture = blueTexture;
            pointerRenderer.sprite = pointer4;
        }
    }

    IEnumerator TakeColdDamage()
    {
        particleManager.Chilled();
        float oldDefaultSpeed = defaultSpeed;

        defaultSpeed -= 1;
        speed = defaultSpeed;

        yield return new WaitForSeconds(5);

        defaultSpeed = oldDefaultSpeed;
        speed = defaultSpeed;
    }

    IEnumerator TakeFireDamage()
    {
        particleManager.OnFire();
        bool running = true;
        int counter = 0;

        while (running)
        {
            yield return new WaitForSeconds(1);
            playerManager.health -= 5;
            playerManager.UpdateHealth();
            //UI shake
            GameObject BG = playerManager.uiPanel.gameObject.transform.Find("BG").gameObject;
            StartCoroutine(BG.GetComponent<UIShaker>().UIShake(4f));
            counter++;

            if (counter == 5)
                running = false;
        }
    }

    public void TakeDamage(int damage, Transform otherPlayer, bool fireDamage, bool chill, bool explode)
    {
        //deal damage

        int damageTaken = damage - perks.damageResistance;
        if (damageTaken < 2)
            playerManager.health -= 2;
        else
            playerManager.health -= damageTaken;

        //check the perk triggers
        if (fireDamage)
            StartCoroutine(TakeFireDamage());
        else if (chill)
            StartCoroutine(TakeColdDamage());
        else if (explode)
        {
            //add the function here
        }

        //fix this for the new health system
        //add movespeed and knockback while damaged
        if (playerManager.health <= perks.moveSpeedThreshold && !perks.addedMoreSpeed)
        {
            perks.addedSpeed += perks.moveSPeedWhileDamaged;
            perks.addedMoreSpeed = true;
        }

        if (playerManager.health <= perks.moreKnockbackThreshold && !perks.addedMoreKnockback)
        {
            perks.addedKnockback += perks.moreKnockbackWhileDamaged;
            perks.addedMoreKnockback = true;
        }

        //get stunned
        StunPlayer(1f);

        //set animation trigger for hit animation
        anim.SetTrigger("Hit");

        //update the health ui
        playerManager.UpdateHealth();

        //hit particle
        Instantiate(hitParticle, (transform.position + new Vector3(0f, 1.5f, 0f)), transform.rotation);

        WeaponSystem weaponSystem = otherPlayer.GetComponent<WeaponSystem>();

        //check if not null first
        if (weaponSystem != null)
        {
            if (weaponSystem.anim.runtimeAnimatorController == weaponSystem.unarmedStance)
            {
                Vector3 relativePos = otherPlayer.position - transform.position;

                // the second argument, upwards, defaults to Vector3.up
                Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);

                Instantiate(hitParticle2, (otherPlayer.position + new Vector3(0f, 1.5f, 0f)), Quaternion.Inverse(otherPlayer.rotation));
                Instantiate(dragonParticle, (otherPlayer.position + new Vector3(0f, 1.5f, 0f)), rotation);
            }
        }

        //set static knockback
        Vector3 oppDirection = otherPlayer.position - transform.position;
        oppDirection = -oppDirection.normalized;
        lastHitDirection = oppDirection;
        rb.AddForce((oppDirection + (transform.up * 1.5f)) * GameData.STATICFORCE, ForceMode.Impulse);
        //camera shake
        StartCoroutine(Camera.main.GetComponent<CameraShaker>().CameraShake(0.2f));
        //UI shake
        GameObject BG = playerManager.uiPanel.gameObject.transform.Find("BG").gameObject;
        StartCoroutine(BG.GetComponent<UIShaker>().UIShake(4f));
    }

    public void SetSpawnProtection()
    {
        StartCoroutine(SpawnProtection());
    }

    IEnumerator SpawnProtection()
    {
        Debug.Log("Starting Spawn Protection for Player " + playerManager.playerNumber);
        playerManager.spawnProtectionOn = true;
        hitboxCol.enabled = false;
        rb.isKinematic = true;

        yield return new WaitForSeconds(4);

        Debug.Log("Ending Spawn Protection for Player " + playerManager.playerNumber);
        CancelSpawnProtection();
    }

    public void CancelSpawnProtection()
    {
        Debug.Log("Ending Spawn Protection for Player " + playerManager.playerNumber);
        playerManager.spawnProtectionOn = false;
        hitboxCol.enabled = true;
        rb.isKinematic = false;
        StopCoroutine(SpawnProtection());
    }

    public void SpawnPlayers()
    {
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        if (playerManager.playerNumber == 1)
            gameObject.transform.position = roundController.spawnPoints[0].transform.position;
        if (playerManager.playerNumber == 2)
            gameObject.transform.position = roundController.spawnPoints[1].transform.position;
        if (playerManager.playerNumber == 3)
            gameObject.transform.position = roundController.spawnPoints[2].transform.position;
        if (playerManager.playerNumber == 4)
            gameObject.transform.position = roundController.spawnPoints[3].transform.position;
    }
}
