﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public const int BASEHEALTH = 100;                      //Base health of the player
    public const float SPEED = 7.0f;                        //base speed of the player
    public const float STATICFORCE = 20f;                   //??
    public const float STRAFESPEED = 3.0f;                  //speed at which the player strafes
    public const float GRAVITY = 10f;                       //Gravity used for game world
    public const float JUMPHEIGHT = 3.0f;                   //height the player can jump
    public const float MAXVELOCITYCHANGE = 10.0f;           //??
    public const float ROTATIONLERPSPEED = 12.0f;           //??
    public const float KNOCKBACKFORCE = 175;                //Player base knockback force
    public const float INITIALKNOCKBACKFORCE = 120;         //??
    public const float GRAPPLEFORCE = 250;                  //??
    public const float UPFORCE = 0.6f;                      //??

    //Game mode names
    public const string DEATHMATCH = "Deathmatch";          //String used to determine Game Mode Deathmatch
    public const string TEAMDEATHMATCH = "TeamDeathmatch";  //String used to determine Game Mode Team Deathmatch
    public const string SPRINTER = "Sprinter";              //String used to determine Game Mode Sprinter
    public const string DODGEBALL = "Dodgeball";            //String used to determine Game Mode Dodgeball

    //perk values
    public const float LESSKNOCKBACK = 0.5f;                //how much less knockback you take
    public const float DAMAGERESISTANCE = 2;                //reduces flat damage   
    public const float MOVESPEEDTHRESHOLD = 90f;            //value health needs to fall below before perk activates
    public const float MOVESPEEDTHRESHOLDCHANGE = 10;       //value the threshold is reduced by
    public const float MOREKNOCKBACKTHRESHOLD = 90f;        //value health needs to fall below before perk activates
    public const float MOREKNOCKBACKTHRESHOLDCHANGE = 10;   //value the threshold is reduced by
    public const float ADDEDDAMAGE = 4f;                    //amount of damage to add to attacks
    public const float ADDEDSPEED = 1f;                     //amount of speed added to player
    public const float ADDEDHEALTH = 20;                    //amount of health to add to player
    public const float ADDEDKNOCKBACK = 1.5f;               //adds to the amount of knockback the player does
    public const float CHANCETODOTONHITCHANGE = 20;         //chance to activate ability per hit (Base = 0)
    public const float CHANCETOSLOWONHITCHANGE = 20;        //chance to activate ability per hit (Base = 0)
    public const float CHANCETOEXPLODEONHITCHANGE = 20;     //chance to activate ability per hit (Base = 0)
    public const float EXTRALIFECHANCECHANGE = 5;           //chance to activate ability per hit (Base = 0)

    //Rope Variables
    public const float WORLDGRABEDLIFETIME = 1;             //duration that the grapple hook remains on the ground
}