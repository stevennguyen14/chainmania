﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour
{
    #region Varibales
    //GM instance used to keep track of the core game stats and functions
    public static GM instance;

    public GameObject[] players;

    //POSSIBLE STATS TO KEEP TRACK OF
    [Header("Stats")]
    public int playerCount;
    public int roundTotalScore;
    public int round;
    public int mapNumber;
    public string winnerName;
    public int winnerScore;

    //player
    public bool player1Playing;
    public bool player2Playing;
    public bool player3Playing;
    public bool player4Playing;

    //map
    public string mapName;

	//perks
	[System.NonSerialized]
	public string[] lowPerks = { "Beef", "Titan","Danger Haste","Danger Impact"};
	[System.NonSerialized]
	public string[] midPerks = {"Hard Hitter","Haste", "Resilience", "Impact"};
	[System.NonSerialized]
	public string[] highPerks = {"Inferno","Ice Cold","Explosive Touch", "Phoenix" };

    //GameModes
    public bool singleMap = false;
    public bool multiMap = false;

    //GameState
    public bool perk;
    public int score;
    public int lives;
    public string gameMode;

    #endregion

    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        //else
        //    Destroy(gameObject);
    }

    void Start()
    {
		//set basic stats to 0 at start
		playerCount = 0;
        roundTotalScore = 0;
        round = 0;
        mapNumber = 0;
        winnerName = "";
        winnerScore = 0;

        //REMOVE WHEN WORKING
        gameMode = GameData.DEATHMATCH;
    }

    #region Player Lobby Settings
    //functions to remember who is playing and which controlle they are
    //this is done so we dont run into controller and player mixups
    public void Player1Playing() { player1Playing = true; }
    public void Player2Playing() { player2Playing = true; }
    public void Player3Playing() { player3Playing = true; }
    public void Player4Playing() { player4Playing = true; }

    //clear the playing for new lobby
    public void ResetPlaying()
    {
        player1Playing = false;
        player2Playing = false;
        player3Playing = false;
        player4Playing = false;
    }

    #endregion

    #region Game State
    public void SetGameState(bool perks, int goalScore, int numberOfLives, string gameModeName)
    {
        perk = perks;
        score = goalScore;
        lives = numberOfLives;
        gameMode = gameModeName;
    }

    public void ClearGameState()
    {
        perk = false;
        score = 0;
        lives = 0;
        gameMode = "";
    }
    #endregion
}
