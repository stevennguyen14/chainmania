﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    #region Variables
    Camera cam;

    //cam movement
    public Transform menuPosition;
    public Transform lobbyPosition;
    public Transform mapSelectPosition;
    public Transform gameOptionsLobbyPosition;

    //PLAYERS
    public MenuPlayer player1;
    public MenuPlayer player2;
    public MenuPlayer player3;
    public MenuPlayer player4;

    //MAIN MENU
    [Header ("UI Canvas")]
    public GameObject mainMenu;
    public GameObject lobbyMenu;
    public GameObject optionsMenu;
    public GameObject gameOptionMenu;

	//GAME LOBBY
	[Header("Game Lobby")]
	public Sprite aButton;
	public Sprite bButton;

	public SpriteRenderer player1Button;
	public SpriteRenderer player2Button;
	public SpriteRenderer player3Button;
	public SpriteRenderer player4Button;

	public TextMeshProUGUI player1PressAText;
    public TextMeshProUGUI player2PressAText;
    public TextMeshProUGUI player3PressAText;
    public TextMeshProUGUI player4PressAText;

    public TextMeshProUGUI player1NameText;
    public TextMeshProUGUI player2NameText;
    public TextMeshProUGUI player3NameText;
    public TextMeshProUGUI player4NameText;

    public TextMeshProUGUI player1ReadyText;
    public TextMeshProUGUI player2ReadyText;
    public TextMeshProUGUI player3ReadyText;
    public TextMeshProUGUI player4ReadyText;

    public GameObject player1Character;
    public GameObject player2Character;
    public GameObject player3Character;
    public GameObject player4Character;

    public GameObject playerJoinScreen;
    public GameObject mapSelectScreen;

    //OPTIONS MENU

    [Header("Testing")]
    public bool player1Ready;
    public bool player2Ready;
    public bool player3Ready;
    public bool player4Ready;

    public bool player1Joined;
    public bool player2Joined;
    public bool player3Joined;
    public bool player4Joined;

    //LOADING
    public GameObject loadingCanvas;
    public GameObject spinner;
    public GameObject pressA;

    //cam
    public Transform target;

    bool reachedTarget = false;
    float speed = 2.5f;

    private AudioSource audioSource;
	private AudioClip joinSound, readySound;

    int howtoCounter = 0;
    public GameObject perkTutScreen;
    public GameObject ControllersMapping;
    public GameObject howToPlay;
    public GameObject hitParticle;

    public bool loading = false;
    public bool a = false;
    #endregion

    #region Start/Update
    void Start()
    {
        GM.instance.ResetPlaying();

        loadingCanvas.SetActive(false);

        howtoCounter = 0;

        cam = Camera.main;
        target = menuPosition;

        //Main Menu opens first
        mainMenu.SetActive(true);
        player1.gameObject.SetActive(true);

        mainMenu.gameObject.transform.Find("StartButton").GetComponent<Button>().Select();

		audioSource = Camera.main.GetComponent<AudioSource>();
		joinSound = Resources.Load("join") as AudioClip;
		readySound = Resources.Load("ready") as AudioClip;


		lobbyMenu.SetActive(false);
        optionsMenu.SetActive(false);
        

        //set all text to default
        PressATextReset();
    }

    void Update()
    {
        if (TargetDistance(cam.transform.gameObject, target.gameObject) >= 0.09f)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, target.position, Time.deltaTime * speed);
            cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, target.rotation, Time.deltaTime * speed);
        }

        //only run if the the lobby meny is open
        if (lobbyMenu.gameObject.activeSelf || loadingCanvas.activeSelf)
        {
            player1.gameObject.SetActive(true);
            player2.gameObject.SetActive(true);
            player3.gameObject.SetActive(true);
            player4.gameObject.SetActive(true);
        }
        else
        {
            player1.gameObject.SetActive(true);
            player2.gameObject.SetActive(false);
            player3.gameObject.SetActive(false);
            player4.gameObject.SetActive(false);
        }
    }
    #endregion

    #region StartGameLobby
    public void StartButton()
    {
        mainMenu.SetActive(false);
        lobbyMenu.SetActive(true);

        target = lobbyPosition;

        optionsMenu.SetActive(false);
    }

    //open options menu
    public void OptionsButton()
    {
        mainMenu.SetActive(false);
        lobbyMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    //Exit game
    public void ExitButton()
    {
        Application.Quit();
    }
    #endregion

    #region Lobby Controls
    public void GoBackFromLobby()
    {
        player1.gameObject.SetActive(true);
        player2.gameObject.SetActive(false);
        player3.gameObject.SetActive(false);
        player4.gameObject.SetActive(false);

        PressATextReset();

        player1NameText.gameObject.SetActive(false);
        player2NameText.gameObject.SetActive(false);
        player3NameText.gameObject.SetActive(false);
        player4NameText.gameObject.SetActive(false);

        player1Character.SetActive(false);
        player2Character.SetActive(false);
        player3Character.SetActive(false);
        player4Character.SetActive(false);

        player1Ready = false;
        player2Ready = false;
        player3Ready = false;
        player4Ready = false;

        player1Joined = false;
        player2Joined = false;
        player3Joined = false;
        player4Joined = false;

        player1Button.sprite = aButton;
        player2Button.sprite = aButton;
        player3Button.sprite = aButton;
        player4Button.sprite = aButton;

        player1ReadyText.gameObject.SetActive(false);
        player2ReadyText.gameObject.SetActive(false);
        player3ReadyText.gameObject.SetActive(false);
        player4ReadyText.gameObject.SetActive(false);

        target = menuPosition;

        player2.NotJoined();
        player3.NotJoined();
        player4.NotJoined();

        //set players to not joined and not ready 

        mainMenu.SetActive(true);
        lobbyMenu.SetActive(false);
        optionsMenu.SetActive(false);

        GM.instance.ResetPlaying();

        StartCoroutine(SelectButton());        
    }

    public void GoBackFromOptionsMenu()
    {
        playerJoinScreen.SetActive(true);

        player1.gameObject.SetActive(true);
        player2.gameObject.SetActive(false);
        player3.gameObject.SetActive(false);
        player4.gameObject.SetActive(false);

        PressATextReset();

        player1NameText.gameObject.SetActive(false);
        player2NameText.gameObject.SetActive(false);
        player3NameText.gameObject.SetActive(false);
        player4NameText.gameObject.SetActive(false);

        player1Character.SetActive(false);
        player2Character.SetActive(false);
        player3Character.SetActive(false);
        player4Character.SetActive(false);

        player1Ready = false;
        player2Ready = false;
        player3Ready = false;
        player4Ready = false;

        player1Joined = false;
        player2Joined = false;
        player3Joined = false;
        player4Joined = false;

        player1ReadyText.gameObject.SetActive(false);
        player2ReadyText.gameObject.SetActive(false);
        player3ReadyText.gameObject.SetActive(false);
        player4ReadyText.gameObject.SetActive(false);

        player1Button.sprite = aButton;
        player2Button.sprite = aButton;
        player3Button.sprite = aButton;
        player4Button.sprite = aButton;

        player1.NotJoined();
        player2.NotJoined();
        player3.NotJoined();
        player4.NotJoined();

        player1.NotReady();

        target = lobbyPosition;

        //set players to not joined and not ready 

        mainMenu.SetActive(false);

        optionsMenu.SetActive(false);

        GM.instance.ResetPlaying();

        StartCoroutine(SelectButton());
    }

    public void GoBackFromMapSelect()
    {
        mapSelectScreen.SetActive(false);

        gameOptionMenu.SetActive(true);

        target = gameOptionsLobbyPosition;    

        /*
        playerJoinScreen.SetActive(true);

        player1.gameObject.SetActive(true);
        player2.gameObject.SetActive(false);
        player3.gameObject.SetActive(false);
        player4.gameObject.SetActive(false);

        PressATextReset();

        player1NameText.gameObject.SetActive(false);
        player2NameText.gameObject.SetActive(false);
        player3NameText.gameObject.SetActive(false);
        player4NameText.gameObject.SetActive(false);

        player1Character.SetActive(false);
        player2Character.SetActive(false);
        player3Character.SetActive(false);
        player4Character.SetActive(false);

        player1Ready = false;
        player2Ready = false;
        player3Ready = false;
        player4Ready = false;

        player1Joined = false;
        player2Joined = false;
        player3Joined = false;
        player4Joined = false;

        player1ReadyText.gameObject.SetActive(false);
        player2ReadyText.gameObject.SetActive(false);
        player3ReadyText.gameObject.SetActive(false);
        player4ReadyText.gameObject.SetActive(false);

        player1Button.sprite = aButton;
        player2Button.sprite = aButton;
        player3Button.sprite = aButton;
        player4Button.sprite = aButton;

        player1.NotJoined();
        player2.NotJoined();
        player3.NotJoined();
        player4.NotJoined();

        player1.NotReady();

        target = lobbyPosition;

        //set players to not joined and not ready 

        mainMenu.SetActive(false);

        optionsMenu.SetActive(false);

        GM.instance.ResetPlaying();

        StartCoroutine(SelectButton());
        */
    }

    IEnumerator SelectButton()
    {
        yield return new WaitForEndOfFrame();
        mainMenu.gameObject.transform.Find("StartButton").GetComponent<Button>().Select();
    }

    float TargetDistance(GameObject obj1, GameObject obj2)
    {
        float distance = Vector3.Distance(obj1.transform.position, obj2.transform.position);
        return distance;
    }

    //resets the help text for all panels
    public void PressATextReset()
    {
        player1PressAText.text = "  To Join";
        player2PressAText.text = "  To Join";
        player3PressAText.text = "  To Join";
        player4PressAText.text = "  To Join";
    }

    //When player 1 joins game change help text and show name and character


    public void StartGame()
    {
        MenuPlayer[] players = FindObjectsOfType(typeof(MenuPlayer)) as MenuPlayer[];

        if (players.Length >= 2)
        {

            int numberOfReady = 0;
            int numberOfJoined = 0;

            foreach (MenuPlayer player in players)
            {
                if (player.joined)
                    numberOfJoined++;
            }

            foreach (MenuPlayer player in players)
            {
                if (player.ready)
                    numberOfReady++;
            }

            if (numberOfReady == numberOfJoined && numberOfReady > 1)
            {
                if (player1.ready) GM.instance.Player1Playing();
                if (player2.ready) GM.instance.Player2Playing();
                if (player3.ready) GM.instance.Player3Playing();
                if (player4.ready) GM.instance.Player4Playing();

                playerJoinScreen.SetActive(false);  
                mapSelectScreen.SetActive(true);

                target = mapSelectPosition;

                mapSelectScreen.transform.Find("Map 1").GetComponent<Button>().Select();
            }
                

        }

    }
    #endregion

    #region Player Lobby Join/Leave
    //input check for players to join or leave USED FOR TESTING WITHOUT CONTROLLERS

    public void Player1Join()
    {
        audioSource.PlayOneShot(joinSound);
        player1PressAText.text = "  to Leave";
        player1Button.sprite = bButton;
        player1NameText.gameObject.SetActive(true);
        player1Character.SetActive(true);
        player1.gameObject.SetActive(true);
        player1.Joined();
    }

    //show ready text
    public void Player1Ready()
    {
        audioSource.PlayOneShot(readySound);
        player1ReadyText.gameObject.SetActive(true);
        player1.Ready();
    }

    //reset join panel
    public void Player1Leave()
    {
        player1PressAText.text = "  To Join";
        player1Button.sprite = aButton;
        player1NameText.gameObject.SetActive(false);
        player1ReadyText.gameObject.SetActive(false);
        player1Character.gameObject.SetActive(false);
        player1.NotReady();
        player1.NotJoined();
        player1.gameObject.SetActive(true);
    }

    //When player 2 joins game change help text and show name and character
    public void Player2Join()
    {
        audioSource.PlayOneShot(joinSound);
        player2PressAText.text = "  to Leave";
        player2Button.sprite = bButton;
        player2NameText.gameObject.SetActive(true);
        player2Character.SetActive(true);
        player2.gameObject.SetActive(true);
        player2.Joined();
    }

    //show ready text
    public void Player2Ready()
    {
        audioSource.PlayOneShot(readySound);
        player2ReadyText.gameObject.SetActive(true);
        player2.Ready();
    }

    //reset the join panel
    public void Player2Leave()
    {
        player2PressAText.text = "  To Join";
        player2Button.sprite = aButton;
        player2NameText.gameObject.SetActive(false);
        player2ReadyText.gameObject.SetActive(false);
        player2Character.gameObject.SetActive(false);
        player2.NotReady();
        player2.NotJoined();
        player2.gameObject.SetActive(false);
    }

    //When player 3 joins game change help text and show name and character
    public void Player3Join()
    {
        audioSource.PlayOneShot(joinSound);
        player3PressAText.text = "  to Leave";
        player3Button.sprite = bButton;
        player3NameText.gameObject.SetActive(true);
        player3Character.SetActive(true);
        player3.gameObject.SetActive(true);
        player3.Joined();
    }

    //show ready text
    public void Player3Ready()
    {
        audioSource.PlayOneShot(readySound);
        player3ReadyText.gameObject.SetActive(true);
        player3.Ready();
    }

    //reset the join panel
    public void Player3Leave()
    {
        player3PressAText.text = "  To Join";
        player3Button.sprite = aButton;
        player3NameText.gameObject.SetActive(false);
        player3ReadyText.gameObject.SetActive(false);
        player3Character.gameObject.SetActive(false);
        player3.NotReady();
        player3.NotJoined();
        player3.gameObject.SetActive(false);
    }

    //When player 4 joins game change help text and show name and character
    public void Player4Join()
    {
        audioSource.PlayOneShot(joinSound);
        player4PressAText.text = "  to Leave";
        player4Button.sprite = bButton;
        player4NameText.gameObject.SetActive(true);
        player4Character.SetActive(true);
        player4.gameObject.SetActive(true);
        player4.Joined();
    }

    //show the ready text
    public void Player4Ready()
    {
        audioSource.PlayOneShot(readySound);
        player4ReadyText.gameObject.SetActive(true);
        player4.Ready();
    }

    //reset the join panel
    public void Player4Leave()
    {
        player4PressAText.text = "  To Join";
        player4Button.sprite = aButton;
        player4NameText.gameObject.SetActive(false);
        player4ReadyText.gameObject.SetActive(false);
        player4Character.gameObject.SetActive(false);
        player4.NotReady();
        player4.NotJoined();
        player4.gameObject.SetActive(false);
    }
    #endregion

    #region MapSelect 
    public void Map1()
    {
        GM.instance.mapName = "Map_1";
        lobbyMenu.SetActive(false);
        mainMenu.SetActive(false);
        loadingCanvas.SetActive(true);
        StartCoroutine(LoadNewScene("GameScene"));
    }

    public void Map2()
    {
        GM.instance.mapName = "Map_2";
        SceneManager.LoadScene("GameScene");
        mainMenu.SetActive(false);
        loadingCanvas.SetActive(true);
        StartCoroutine(LoadNewScene("GameScene"));
    }

    public void Map3()
    {
        GM.instance.mapName = "Map_3";
        lobbyMenu.SetActive(false);
        mainMenu.SetActive(false);
        loadingCanvas.SetActive(true);
        StartCoroutine(LoadNewScene("GameScene"));
    }

    public void Map4()
    {
        GM.instance.mapName = "Map_4";
        lobbyMenu.SetActive(false);
        mainMenu.SetActive(false);
        loadingCanvas.SetActive(true);
        StartCoroutine(LoadNewScene("GameScene"));
    }

    #endregion

    #region Load New Scene
    IEnumerator LoadNewScene(string sceneName)
    {
        loading = true;
        spinner.SetActive(true);
        pressA.SetActive(false);

        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            if (async.progress >= 0.9f)
            {
                spinner.SetActive(false);
                pressA.SetActive(true);
                loading = false;

                if (a)
                {
                    async.allowSceneActivation = true;
                }
            }
            yield return null;
        }    
    }

    public void PressedALoading()
    {
            a = true;
    }
    #endregion

    #region TutorialScreen
    public void NextHowTo()
    {
        if (howtoCounter == 0)
        {
            howtoCounter++;
            ControllersMapping.SetActive(true);
            perkTutScreen.SetActive(false);
        }
        else if (howtoCounter == 1)
        {
            howtoCounter++;
            ControllersMapping.SetActive(false);
            
            perkTutScreen.SetActive(true);
        }
        else
        {
            perkTutScreen.SetActive(false);
            howToPlay.SetActive(false);
            ControllersMapping.SetActive(false);
            optionsMenu.SetActive(false);
            mainMenu.SetActive(true);
            player1.gameObject.SetActive(true);
            StartCoroutine(SelectButton());
            howtoCounter = 0;
        }

    }
    #endregion

    #region MenuParticles
    public void Player1Sparkle()
    {
        Instantiate(hitParticle, (player1ReadyText.gameObject.transform.position + new Vector3(0f, 1.5f, 0f)), transform.rotation);
    }

    public void Player2Sparkle()
    {
        Instantiate(hitParticle, (player2ReadyText.gameObject.transform.position + new Vector3(0f, 1.5f, 0f)), transform.rotation);
    }

    public void Player3Sparkle()
    {
        Instantiate(hitParticle, (player3ReadyText.gameObject.transform.position + new Vector3(0f, 1.5f, 0f)), transform.rotation);
    }

    public void Player4Sparkle()
    {
        Instantiate(hitParticle, (player4ReadyText.gameObject.transform.position + new Vector3(0f, 1.5f, 0f)), transform.rotation);
    }
    #endregion
}
