﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSoundFX : MonoBehaviour
{
	public AudioClip selectSound, confirmSound;

	private AudioSource audioSource;
	private string jumpString;
    // Start is called before the first frame update
    void Start()
    {
		audioSource = Camera.main.GetComponent<AudioSource>();
		jumpString = "JumpP1";
	}

	void Update()
	{
		if (Input.GetButtonDown(jumpString))
		{
			Confirm();
		}
	}

    void Confirm()
	{
		audioSource.PlayOneShot(confirmSound);
	}

	public void Select()
	{
		audioSource.PlayOneShot(selectSound);
	}
}
