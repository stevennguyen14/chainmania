﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayer : MonoBehaviour
{
    #region Variables
    public bool ready;
    public bool joined;
    public int playerNumber;

    //Inpout Strings
    public string aButton;
    public string bButton;
    public string startButton;

    public MenuManager menuManager;
    bool buttonPressed;

    #endregion

    #region Start/Update
    // Start is called before the first frame update
    void OnEnable()
    {
        ready = false;
        SetPlayerNumber();

        aButton = "JumpP" + playerNumber;
        bButton = "ThrowP" + playerNumber;
        startButton = "StartButtonP" + playerNumber;
    }

    void Update()
    {
        //only player 1 can back out of menu
        if (playerNumber == 1)
        {
            //if player pushed button then go back a menu and disable the players
            if (menuManager.lobbyMenu.gameObject.activeSelf)
            {
                if (menuManager.target == menuManager.lobbyPosition && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.Backspace)))
                    menuManager.GoBackFromLobby();
                else if (menuManager.target == menuManager.lobbyPosition && (Input.GetButtonDown(startButton) || Input.GetKeyDown(KeyCode.Return)))
                    menuManager.StartGame();
                else if (menuManager.target == menuManager.gameOptionsLobbyPosition && (Input.GetButtonDown(startButton) || Input.GetKeyDown(KeyCode.Return)))
                {
                    //if (menuManager.buttonSelected)
                        //menuManager.SetOptions();
                }
                else if (menuManager.target == menuManager.gameOptionsLobbyPosition && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.Backspace)))
                    menuManager.GoBackFromOptionsMenu();                
                else if (menuManager.target == menuManager.mapSelectPosition && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.Backspace)))
                    menuManager.GoBackFromMapSelect();
            }


            if (menuManager.optionsMenu.gameObject.activeSelf)
            {
                if ((Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.Return)) && !buttonPressed)
                {
                    menuManager.NextHowTo();
                    buttonPressed = true;
                    StartCoroutine(DelayPress());
                }
            }
        }

        //used for joining and readying up
        PlayerLobby();

        if (menuManager.target == menuManager.lobbyPosition && Input.GetButtonDown(aButton) && joined)
        {
            if (playerNumber == 1)
                menuManager.Player1Sparkle();
            else if (playerNumber == 2)
                menuManager.Player2Sparkle();
            else if (playerNumber == 3)
                menuManager.Player3Sparkle();
            else if (playerNumber == 4)
                menuManager.Player4Sparkle();
        }

        //input for loading screen
        if (menuManager.loadingCanvas.activeSelf)
        {
            if (!menuManager.loading)
            {
                if (Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.Return))
                {
                    menuManager.PressedALoading();
                }
                    
            }
        }

    }
    #endregion

    #region Player Lobby Controls
    void PlayerReadyInput()
    {
        switch (playerNumber)
        {
            case 1:
                if (menuManager.player1Joined == true && menuManager.player1Ready == false)
                {
                    menuManager.player1Ready = true;
                    menuManager.Player1Ready();
                }
                break;
            case 2:
                if (menuManager.player2Joined == true && menuManager.player2Ready == false)
                {
                    menuManager.player2Ready = true;
                    menuManager.Player2Ready();
                }
                break;
            case 3:
                if (menuManager.player3Joined == true && menuManager.player3Ready == false)
                {
                    menuManager.player3Ready = true;
                    menuManager.Player3Ready();
                }
                break;
            case 4:
                if (menuManager.player4Joined == true && menuManager.player4Ready == false)
                {
                    menuManager.player4Ready = true;
                    menuManager.Player4Ready();
                }
                break;
        }
    }

    void PlayerJoinAndLeave()
    {
        switch (playerNumber)
        {
            case 1:
                if (menuManager.player1Joined == false)
                {
                    if (Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.A))
                    {
                        menuManager.player1Joined = true;
                        menuManager.Player1Join();
                    }
                }
                else
                {
                    if (menuManager.player1Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.Q)))
                    {
                        menuManager.player1Ready = false;
                        menuManager.player1ReadyText.gameObject.SetActive(false);
                    }
                    else if (!menuManager.player1Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.Q)))
                    {
                        menuManager.player1Joined = false;
                        menuManager.Player1Leave();
                    }
                }
                break;
            case 2:
                if (menuManager.player2Joined == false)
                {
                    if (Input.GetButtonDown(aButton)|| Input.GetKeyDown(KeyCode.S))
                    {
                        menuManager.player2Joined = true;
                        menuManager.Player2Join();
                    }
                }
                else
                {
                    if (menuManager.player2Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.W)))
                    {
                        menuManager.player2Ready = false;
                        menuManager.player2ReadyText.gameObject.SetActive(false);
                    }
                    else if (!menuManager.player2Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.W)))
                    {
                        menuManager.player2Joined = false;
                        menuManager.Player2Leave();
                    }
                }
                break;
            case 3:
                if (menuManager.player3Joined == false)
                {
                    if (Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.D))
                    {
                        menuManager.player3Joined = true;
                        menuManager.Player3Join();
                    }
                }
                else
                {
                    if (menuManager.player3Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.E)))
                    {
                        menuManager.player3Ready = false;
                        menuManager.player3ReadyText.gameObject.SetActive(false);
                    }
                    else if (!menuManager.player3Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.E)))
                    {
                        menuManager.player3Joined = false;
                        menuManager.Player3Leave();
                    }
                }
                break;
            case 4:
                if (menuManager.player4Joined == false)
                {
                    if (Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.F))
                    {
                        menuManager.player4Joined = true;
                        menuManager.Player4Join();
                    }
                }
                else
                {
                    if (menuManager.player4Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.R)))
                    {
                        menuManager.player4Ready = false;
                        menuManager.player4ReadyText.gameObject.SetActive(false);
                    }
                    else if (!menuManager.player4Ready && (Input.GetButtonDown(bButton) || Input.GetKeyDown(KeyCode.R)))
                    {
                        menuManager.player4Joined = false;
                        menuManager.Player4Leave();
                    }
                }
                break;
        }
    }

    IEnumerator DelayPress()
    {
        yield return new WaitForSeconds(1);
        buttonPressed = false;
    }

    void PlayerLobby()
    {
        if ((Input.GetKeyDown(KeyCode.S) || Input.GetButtonDown(aButton) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.F)))
            PlayerReadyInput();

        PlayerJoinAndLeave();
    }

    #endregion

    #region Lobby Checks
    public void Ready()
    {
        ready = true;
    }

    public void NotReady()
    {
        ready = false;
    }

    public void Joined()
    {
        joined = true;
    }

    public void NotJoined()
    {
        joined = false; 
    }

    #endregion

    #region PlayerNumbers
    public void SetPlayerNumber()
    {
        if (gameObject.name == "Player 1")
            playerNumber = 1;

        if (gameObject.name == "Player 2")
            playerNumber = 2;

        if (gameObject.name == "Player 3")
            playerNumber = 3;

        if (gameObject.name == "Player 4")
            playerNumber = 4;
    }
    #endregion
}
