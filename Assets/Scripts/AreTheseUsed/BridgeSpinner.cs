﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeSpinner : MonoBehaviour
{

	private bool spin;
	private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(WaitBeforeSpin());
		rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		if (spin)
			rb.AddTorque(transform.up * 100f);
		//transform.Rotate(Vector3.up * 10f * Time.deltaTime);
	}

	IEnumerator WaitBeforeSpin()
	{
		yield return new WaitForSeconds(3f);
		spin = true;
	}
}
