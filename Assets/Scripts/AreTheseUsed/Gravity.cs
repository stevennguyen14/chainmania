﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Rotates a this transform to align it towards the target transform's position.
/// </summary>
public class Gravity : MonoBehaviour{

	[SerializeField] Transform planet;
	[SerializeField] float gravityForce;
	private Vector3 lookDirection;
	private Rigidbody rb;

	private void Start()
	{
		rb = gameObject.GetComponent<Rigidbody>();
	}

	void FixedUpdate(){
		Vector3 dir = (transform.position - planet.position).normalized;
		RotateGravity(dir);
		transform.rotation = Quaternion.FromToRotation(transform.up, dir) * transform.rotation;
		rb.AddForce((transform.position - planet.position) * -gravityForce);
	}

	void RotateGravity(Vector3 up)
	{
		lookDirection = Quaternion.FromToRotation(transform.up, up) * lookDirection;
	}
}